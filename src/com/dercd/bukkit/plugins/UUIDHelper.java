package com.dercd.bukkit.plugins;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerResolveException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;
import com.dercd.bukkit.cdapi.tools.minecraft.NameFetcher;
import com.dercd.bukkit.cdapi.tools.minecraft.UUIDFetcher;

public class UUIDHelper extends CDPlugin
{
	NameFetcher nf;
	UUIDFetcher uf;
	Log clog;
	public final static String mbeg = ChatColor.DARK_GREEN + "[" + ChatColor.GOLD + "UUIDHelper" + ChatColor.DARK_GREEN + "] " + ChatColor.WHITE;

	public UUIDHelper(PluginHandler handler)
	{
		super(handler);
		this.nf = handler.getNameFetcher();
		this.uf = handler.getUUIDFetcher();
		this.clog = handler.getCLog();
	}
	
	@Override
	public String getVersion()
	{
		return "0.1.1-141121-215620";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@Override
	public Permission[] getPermissions()
	{
		return new Permission[]
		{
			new Permission("cd.uuh", PermissionDefault.OP)
		};
	}
	
	@CDPluginCommand(commands = { "uuidhelper cd.uuh" })
	public void onCommand(CommandEvent e) throws CDInvalidArgsException
	{
		e.validateCount(1, -1);
		String[] args = e.getArgs();
		UUID uuid;
		String name;
		int i;
		String[] subCommand;
		switch(args[0].toLowerCase())
		{
			case "fetchuuid":
				e.validateCount(2);
				try
				{
					uuid = this.uf.fetch(args[1]);
					e.getSender().sendMessage(mbeg + "Player " + ChatColor.GOLD + args[1] + ChatColor.WHITE + " has the UUID " + ChatColor.GOLD + uuid.toString());
				}
				catch(CDPlayerResolveException x) { x.handle(this.handler, e); }
				return;
			case "fetchname":
				e.validateCount(2);
				try
				{
					name = this.nf.fetch(UUID.fromString(args[1]));
					e.getSender().sendMessage(mbeg + "The Player associated with the UUID " + ChatColor.GOLD + args[1] + ChatColor.WHITE + " has the name " + ChatColor.GOLD + name);
				}
				catch(CDPlayerResolveException x) { x.handle(this.handler, e); }
				return;
			case "replacename":
				e.validateCount(3, -1);
				e.validateInt(1);
				i = Integer.valueOf(args[1]) - 1;
				subCommand = Var.subArr(args, 2);
				try
				{
					subCommand[i] = this.uf.fetch(subCommand[i]).toString();
				}
				catch(CDPlayerResolveException x) { x.handle(this.handler, e); return; }
				if(subCommand[0].startsWith("/"))
					Bukkit.dispatchCommand(e.getSender(), Var.arrToString(subCommand, 0).substring(1));
				else if(!e.isSenderConsole())
					((Player) e.getSender()).chat(Var.arrToString(subCommand, 0));
				return;
			case "replaceuuid":
				e.validateCount(3, -1);
				e.validateInt(1);
				i = Integer.valueOf(args[1]) - 1;
				subCommand = Var.subArr(args, 2);
				try
				{
					if(!Var.isUUID(subCommand[i]))
						throw new CDInvalidArgsException();
					subCommand[i] = this.nf.fetch(UUID.fromString(subCommand[i])).toString();
				}
				catch(CDPlayerResolveException x) { x.handle(this.handler, e); return; }
				if(subCommand[0].startsWith("/"))
					Bukkit.dispatchCommand(e.getSender(), Var.arrToString(subCommand, 0).substring(1));
				else if(!e.isSenderConsole())
					((Player) e.getSender()).chat(Var.arrToString(subCommand, 0));
				return;
		}
		throw new CDInvalidArgsException();
	}
	
	@CDPluginEvent
	public void onJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		this.clog.log(p.getName() + " joins the game. He/She has the UUID " + p.getUniqueId(), this);
		this.nf.put(p.getUniqueId(), p.getName());
	}

	@CDPluginEvent
	public void onQuit(PlayerQuitEvent e)
	{
		Player p = e.getPlayer();
		this.clog.log(p.getName() + "(" + p.getUniqueId().toString() + ") leaves the server", this);
	}
}

package com.dercd.bukkit.plugins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.events.CDPluginEnableEvent;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class Man extends CDPlugin
{
	Log clog;
	private ManEntry manEntries = new ManEntry("man", "Top element");
	public Map<String, List<String>> help = new HashMap<String, List<String>>();
	public static final String mbeg = ChatColor.GRAY + "[MAN] ";

	public Man(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}
	
	@Override
	public String getVersion()
	{
		return "0.2";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@CDPluginEvent
	public void onEnable(CDPluginEnableEvent e)
	{
		this.clog.log("Loading mans from the plugins", this);
		ManEntry[] me;
		for(CDPlugin cdp : this.handler.plugins.values())
			if((me = cdp.man()) != null)
				for(ManEntry man : me)
					this.manEntries.addEntry(man);
		this.clog.log("All mans loaded", this);
	}
	
	@Override
	public ManEntry[] man()
	{
		ManEntry back = new ManEntry("man", "MAN");
		back.addEntry("default", "<cmd>");
		back.addEntry("book", "<cmd> book");
		return new ManEntry[] { back };
	}
	
	@Override
	public Permission[] getPermissions()
	{
		return new Permission[]
		{
			new Permission("cd.man", PermissionDefault.OP),
			new Permission("cd.man.load", PermissionDefault.OP)
		};
	}

	@CDPluginCommand(commands = { "man cd.man 0" })
	public void onCommand(CommandEvent e) throws CDInvalidArgsException
	{
		e.validateCount(1, 2);
		if(e.getArgs().length == 1)
			printUsage(e.getSender(), e.getArgs()[0]);
		else if(e.getArgs()[1].equalsIgnoreCase("book"))
		{
			Player p = (Player) e.getSender();
			if(p.getInventory().firstEmpty() == -1)
			{
				p.sendMessage(mbeg + "Your inventory is full. No space for a book");
				return;
			}
			p.getInventory().addItem(createBook(p, e.getArgs()[0]));
		}
	}

	public void printUsage(CommandSender sender, String id)
	{
		ManEntry manEntry = this.manEntries.getEntry(id);
		if (manEntry == null)
			sender.sendMessage(mbeg + "No Help for this command available");
		else for (String line : manEntry.buildList())
			sender.sendMessage(mbeg + line);
	}
	public ItemStack createBook(CommandSender sender, String id)
	{
		ManEntry manEntry = this.manEntries.getEntry(id);
		if (manEntry == null)
		{
			sender.sendMessage(mbeg + "No Help for this command available");
			return null;
		}
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta meta = (BookMeta) book.getItemMeta();
		StringBuilder sb = new StringBuilder();
		Class<? extends CDPlugin> clazz = getClass();
		Var.createBool(clazz, "firstRound");
		for (String line : manEntry.buildList())
			if(sb.length() + 1 + line.length() > 256 || sb.toString().split("\n").length >= 10)
			{
				if(meta.getPageCount() >= 50) break;
				meta.addPage(sb.toString());
				sb = new StringBuilder();
				sb.append(line);
			}
			else sb.append((Var.isDoBool(clazz, "firstRound") ? "" : "\n") + line);
		if(sb.length() != 0 && meta.getPageCount() < 50)
			meta.addPage(sb.toString());
		meta.setTitle("" + ChatColor.GRAY + ChatColor.BOLD + ChatColor.ITALIC + "MAN: " + id);
		meta.setAuthor("MAN");
		book.setItemMeta(meta);
		return book;
	}
	
	public boolean hasHelp(String id)
	{
		return this.manEntries.hasEntry(id);
	}	
	
	public static class ManEntry
	{
		private String id;
		private String text;
		private Map<String, ManEntry> entries = new HashMap<String, ManEntry>();
		private ManEntry parent;
		
		
		public ManEntry(String id, String text)
		{
			this(id, text, null);
		}
		public ManEntry(String id, String text, ManEntry parent)
		{
			this.id = id;
			this.text = text;
			this.parent = parent;
		}
		
		public boolean hasParent()
		{
			return this.parent != null;
		}
		public ManEntry getParent()
		{
			return this.parent;
		}
		
		public ManEntry getEntry(String id)
		{
			String[] ids = id.split("\\.");
			ManEntry currentEntry = this.entries.get(ids[0]);
			for(int i = 1; i < ids.length; i++)
			{
				if(currentEntry == null) return null;
				currentEntry = currentEntry.getEntry(ids[i]);
			}
			return currentEntry;
		}
		
		public boolean hasEntry(String id)
		{
			return getEntry(id) != null;
		}
		public boolean hasEntries()
		{
			return this.entries.size() != 0;
		}
		
		public String getId()
		{
			return this.id;
		}
		public String getText()
		{
			return this.text;
		}
		
		private void setParent(ManEntry parent)
		{
			this.parent = parent;
		}
		
		public void addEntry(ManEntry entry)
		{
			entry.setParent(this);
			this.entries.put(entry.getId(), entry);
		}
		public void addEntry(String id, String text)
		{
			String[] ids = id.split("\\.");
			ManEntry currentEntry = ids.length == 1 ? this : this.entries.get(ids[0]);
			for(int i = 1; i + 1 < ids.length; i++)
				currentEntry = currentEntry.getEntry(ids[i]);
			currentEntry.addEntry(new ManEntry(ids[ids.length - 1], text, this));
		}

		public List<String> buildList()
		{
			return buildList(0);
		}
		public List<String> buildList(int depth)
		{
			List<String> back = new ArrayList<String>();
			back.add(Var.expandString(">", depth) + getText());
			for(ManEntry child : this.entries.values())
				back.addAll(child.buildList(depth + 1));
			return back;
		}
		
		public ManEntry clone(String newId, String newText)
		{
			ManEntry copy = new ManEntry(newId, newText, this.parent);
			copy.entries = this.entries;
			return copy;
		}
		@Override
		public ManEntry clone()
		{
			return clone(this.id, this.text);
		}
		public ManEntry deepClone(String newId, String newText)
		{
			ManEntry copy = new ManEntry(newId, newText, this.parent);
			for(ManEntry entry : this.entries.values())
				copy.addEntry(entry.deepClone());
			return copy;
		}
		public ManEntry deepClone()
		{
			return deepClone(this.id, this.text);
		}
	}
}

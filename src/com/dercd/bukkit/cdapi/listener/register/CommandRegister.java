package com.dercd.bukkit.cdapi.listener.register;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.SimpleCommandMap;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.listener.CommandListener;
import com.dercd.bukkit.cdapi.listener.objects.CDCommandObject;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.listener.objects.bukkit.CDCommand;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Data;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CommandRegister
{
	CDAPI cdapi;
	Log clog;
	CommandListener clistener;
	public SimpleCommandMap commandMap;
	public Map<String, Command> knownCommands;
	public Collection<Command> cmds = null;
	private List<String> skip;

	public CommandRegister(CDAPI cdapi)
	{
		this.cdapi = cdapi;
		this.clog = cdapi.getHandler().getCLog();
		this.clistener = cdapi.getHandler().getCommandListener();
		this.commandMap = getCommandMap();
		this.knownCommands = getKnownCommands(this.commandMap);
		this.skip = getSkip();
	}

	private List<String> getSkip()
	{
		File f = new File(CDPlugin.getDir() + "/overridden");
		if (!f.exists()) return new ArrayList<String>();
		String file;
		try
		{
			file = Data.readFile(CDPlugin.getDir() + "/overridden", Charset.forName("UTF-8"));
		}
		catch (Exception x)
		{
			return new ArrayList<String>();
		}
		String[] lines = file.split("(\\r){0,1}(\\n)");
		return Var.toList(lines);
	}

	public void searchCommands(boolean registerToBukkit)
	{
		this.clog.log("Searching Commands", this);
		List<CDListenerObject> list;
		List<String> aliases;
		String simpleCmd;
		String newCmd;
		CDPluginCommand pc;
		for (CDPlugin cdp : this.cdapi.getHandler().plugins.values())
			for (Method m : cdp.getClass().getMethods())
			{
				if ((pc = m.getAnnotation(CDPluginCommand.class)) == null) continue;
				for (String command : pc.commands())
				{
					if ((newCmd = addMissingParts(command)) == null)
					{
						this.clog.log("Commands have to have the Syntax 'COMMAND [PERMISSION] [SHOWMSG(0 | 1)]. '" + command + "' does not have this Syntax and would not be registered.", this);
						continue;
					}
					if ((list = this.clistener.registered.get(newCmd.toLowerCase())) == null) list = new ArrayList<CDListenerObject>();
					list.add(new CDCommandObject(cdp, m, pc.priority(), pc.ignoreCancelled()));
					this.clistener.registered.put(newCmd.toLowerCase(), list);
					if ((aliases = this.clistener.commandList.get((simpleCmd = parseToCommand(newCmd)))) == null) aliases = new ArrayList<String>();
					if(!aliases.contains(newCmd)) aliases.add(newCmd);
					this.clistener.commandList.put(simpleCmd, aliases);
					this.clog.log("Registering '" + newCmd + "' Command to " + cdp.getClass().getSimpleName() + " with priority = " + pc.priority(), this);
				}
			}
		sortCalls();
		this.clog.log("Done", this);
		if (registerToBukkit) registerBukkitCommands();
	}
	
	public void searchCommands(CDPlugin cdp, boolean addToCommandList)
	{
		this.clog.log("Searching Commands in " + cdp.getName(), this);
		List<CDListenerObject> list;
		List<String> aliases;
		String simpleCmd;
		String newCmd;
		CDPluginCommand pc;
		for (Method m : cdp.getClass().getMethods())
		{
			if ((pc = m.getAnnotation(CDPluginCommand.class)) == null) continue;
			for (String command : pc.commands())
			{
				if ((newCmd = addMissingParts(command)) == null)
				{
					this.clog.log("Commands have to have the Syntax 'COMMAND [PERMISSION] [SHOWMSG(0 | 1)]. '" + command + "' does not have this Syntax and would not be registered.", this);
					continue;
				}
				if ((list = this.clistener.registered.get(newCmd.toLowerCase())) == null) list = new ArrayList<CDListenerObject>();
				list.add(new CDCommandObject(cdp, m, pc.priority(), pc.ignoreCancelled()));
				this.clistener.registered.put(newCmd.toLowerCase(), list);
				if(addToCommandList)
				{
					if ((aliases = this.clistener.commandList.get((simpleCmd = parseToCommand(newCmd)))) == null) aliases = new ArrayList<String>();
					if(!aliases.contains(newCmd)) aliases.add(newCmd);
					this.clistener.commandList.put(simpleCmd, aliases);
				}
				this.clog.log("Registering '" + newCmd + "' Command to " + cdp.getClass().getSimpleName() + " with priority = " + pc.priority(), this);
			}
		}
		sortCalls();
		this.clog.log("Done", this);
	}
	
	private void sortCalls()
	{
		this.clog.log("Sorting CommandCalls by Priority", this);
		Var.sortCalls(this.clistener.registered);
		this.clog.log("All Packets with Priority between 0 and 12000 sorted", this);
	}

	private String parseToCommand(String input)
	{
		return input.split(" ")[0];
	}

	private String addMissingParts(String command)
	{
		String[] parts = command.split(" ");
		if (parts.length == 0 || parts.length > 3) return null;
		String[] newParts = new String[] { parts[0], (parts.length == 3 ? parts[1] : "."), (parts.length == 3 ? parts[2] : "1") };
		if (parts.length == 1) return Var.arrToString(newParts, 0);
		if (parts[1].equals("0") || parts[1].equals("1")) newParts[2] = parts[1];
		else newParts[1] = parts[1];
		return Var.arrToString(newParts, 0);
	}

	private List<String> parseToCommands(Set<String> set)
	{
		List<String> back = new ArrayList<String>();
		for (String str : set)
			back.add(parseToCommand(str));
		return back;
	}

	public void unregisterBukkitCommands()
	{
		this.clog.log("Unregister Commands from Bukkit", this);
		Command c;
		for (String cmd : parseToCommands(this.clistener.registered.keySet()))
			if (!this.skip.contains(cmd))
			{
				this.clog.log("Unregistering " + cmd, this);
				if ((c = this.commandMap.getCommand("cdapi:" + cmd)) != null)
				{
					c.unregister(this.commandMap);
					this.knownCommands.remove("cdapi:" + cmd);
				}
				else this.clog.log("Command cdapi:" + cmd + " not found", this);
				if ((c = this.commandMap.getCommand(cmd)) != null)
				{
					c.unregister(this.commandMap);
					this.knownCommands.remove(cmd);
				}
				else this.clog.log("Command " + cmd + " not found", this);
			}
		this.clog.log("Commands unregistered", this);
	}

	public void registerBukkitCommands()
	{
		this.clog.log("Register Commands to Bukkit", this);
		registerBukkitCommands(this.clistener.commandList.keySet());
		this.clog.log("Done", this);
	}

	private void registerBukkitCommands(Set<String> set)
	{
		for (String command : set)
			registerBukkitCommand(command);
	}

	private void registerBukkitCommand(String command)
	{
		if (this.commandMap == null) return;
		if (this.skip.contains(command))
		{
			this.clog.log("Skipping Command " + command + " because he is marked as overridden", this);
			this.commandMap.register("", new CDCommand("cdapi:" + command, this.cdapi));
			return;
		}
		this.clog.log("Registering Command " + command + " to Bukkit", this);
		Command c = this.knownCommands.get(command);
		if (c != null)
		{
			this.clog.log("Unregistering old Command", this);
			c.unregister(this.commandMap);
			this.knownCommands.remove(command);
		}
		this.clog.log("Registering new Command", this);
		this.commandMap.register("cdapi", new CDCommand(command, this.cdapi));
	}

	private SimpleCommandMap getCommandMap()
	{
		this.clog.log("Getting BukkitCommandMap", this);
		try
		{
			return (SimpleCommandMap) Bukkit.getServer().getClass().getMethod("getCommandMap").invoke(Bukkit.getServer());
		}
		catch (Exception x)
		{
			this.clog.log("Error while getting CommandMap", this);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, Command> getKnownCommands(SimpleCommandMap scm)
	{
		this.clog.log("Getting knownCommands", this);
		try
		{
			Field f = scm.getClass().getDeclaredField("knownCommands");
			f.setAccessible(true);
			return (Map<String, Command>) f.get(scm);
		}
		catch (Exception x)
		{
			this.clog.log("Error while getting knownCommands", this);
			return null;
		}
	}

	public void unregisterPlugin(CDPlugin cdp)
	{
		PluginHandler.unregisterPlugin(cdp, this.clistener.registered);
	}
}

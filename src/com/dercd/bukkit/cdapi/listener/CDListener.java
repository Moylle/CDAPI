package com.dercd.bukkit.cdapi.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.tools.Log;

/**
 * The abstract class all listener extend from
 * 
 * @author CD
 *
 */
public abstract class CDListener
{
	Log clog;
	PluginHandler handler;
	/**
	 * The map all Methods and Objects to call are registered in
	 */
	public Map<String, List<CDListenerObject>> registered = new HashMap<String, List<CDListenerObject>>();

	public CDListener(PluginHandler handler)
	{
		this.handler = handler;
		this.clog = handler.getCLog();
	}
	
	/**
	 * 
	 * @param x The exception to be handled
	 * @param wasHandled If the exception was already tried to handle
	 */
	public void handleException(Throwable x, boolean wasHandled, CDListenerObject cdlo)
	{
		this.handler.handleException(x, wasHandled, (CDPlugin) cdlo.getObject());
	}
}

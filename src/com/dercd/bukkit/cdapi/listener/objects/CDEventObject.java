package com.dercd.bukkit.cdapi.listener.objects;

import java.lang.reflect.Method;

import org.bukkit.event.EventPriority;

import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDEventObject extends CDListenerObject
{
	private boolean lateRegistered;
	private boolean ignoreCancelled;
	private EventPriority epriority;

	public CDEventObject(Object o, Method m, int priority, boolean lateRegistered, boolean ignoreCancelled)
	{
		super(o, m, priority);
		this.lateRegistered = lateRegistered;
		this.ignoreCancelled = ignoreCancelled;
		this.epriority = EventPriority.valueOf(Var.getPriority(priority));
	}

	public boolean getLateRegistered()
	{
		return this.lateRegistered;
	}

	public boolean getIgnoreCancelled()
	{
		return this.ignoreCancelled;
	}

	public EventPriority getBukkitPriority()
	{
		return this.epriority;
	}
}

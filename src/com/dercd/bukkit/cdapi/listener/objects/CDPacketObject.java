package com.dercd.bukkit.cdapi.listener.objects;

import java.lang.reflect.Method;

import com.comphenix.protocol.events.ListenerPriority;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDPacketObject extends CDListenerObject
{
	private boolean lateRegistered;
	private boolean ignoreCancelled;
	private ListenerPriority lpriority;

	public CDPacketObject(Object o, Method m, int priority, boolean lateRegistered, boolean ignoreCancelled)
	{
		super(o, m, priority);
		this.lateRegistered = lateRegistered;
		this.ignoreCancelled = ignoreCancelled;
		this.lpriority = ListenerPriority.valueOf(Var.getPriority(priority));
	}

	public boolean getLateRegistered()
	{
		return this.lateRegistered;
	}

	public boolean getIgnoreCancelled()
	{
		return this.ignoreCancelled;
	}

	public ListenerPriority getListenerPriority()
	{
		return this.lpriority;
	}
}

package com.dercd.bukkit.cdapi.listener.objects.bukkit;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.listener.CommandListener;

public class CDCommand extends Command
{
	protected CommandListener clistener;

	public CDCommand(String name, CDAPI cdapi)
	{
		super(name);
		this.clistener = cdapi.getHandler().getCommandListener();
	}

	@Override
	public boolean execute(CommandSender sender, String commandLabel, String[] args)
	{
		this.clistener.onCommand(this, args, sender);
		return true;
	}
}

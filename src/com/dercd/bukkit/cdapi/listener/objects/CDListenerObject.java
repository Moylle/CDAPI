package com.dercd.bukkit.cdapi.listener.objects;

import java.lang.reflect.Method;

public class CDListenerObject
{
	private Object o;
	private Method m;
	private int priority;

	public CDListenerObject(Object o, Method m, int priority)
	{
		this.o = o;
		this.m = m;
		this.priority = priority;
	}

	public Object getObject()
	{
		return this.o;
	}

	public Method getMethod()
	{
		return this.m;
	}

	public int getPriority()
	{
		return this.priority;
	}
}

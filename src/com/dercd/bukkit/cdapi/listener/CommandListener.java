package com.dercd.bukkit.cdapi.listener;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCException;
import com.dercd.bukkit.cdapi.exceptions.CDGException;
import com.dercd.bukkit.cdapi.exceptions.CDNoPermissionException;
import com.dercd.bukkit.cdapi.exceptions.Handleable;
import com.dercd.bukkit.cdapi.listener.objects.CDCommandObject;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.listener.objects.bukkit.CDCommand;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;
import com.dercd.bukkit.plugins.CDAPICommand;

public class CommandListener extends CDListener
{
	private Log clog;
	JavaPlugin cdapi;
	PluginHandler handler;
	private boolean aggressiveCmdListener = false;
	private PacketAdapter aggressiveChatListener;
	public HashMap<String, List<String>> commandList = new HashMap<String, List<String>>();

	public CommandListener(PluginHandler handler)
	{
		super(handler);
		this.handler = handler;
		this.cdapi = handler.getMain();
		this.clog = handler.getCLog();
	}

	public void onCommand(Command c, String[] args, CommandSender sender)
	{
		onCommand(new CommandEvent(c, sender, args));
	}

	public void onCommand(CommandEvent e)
	{
		this.clog.log("Incomming Command '" + e.getCommand().getName() + " " + Var.arrToString(e.getArgs(), 0) + "' from " + e.getSender().getName(), this);
		String name = e.getCommand().getName().toLowerCase();
		if (name.startsWith("cdapi:") && name.length() > 6) name = name.substring(6);
		List<String> keys;
		List<CDListenerObject> los;
		try
		{
			if ((keys = this.commandList.get(name)) != null)
				for (String key : keys)
				{
					checkFullCommand(key, e);
					if ((los = this.registered.get(key)) != null)
						for (CDListenerObject o : los)
							try
							{
								try
								{
									if(e.isCancelled() && !((CDCommandObject) o).getIgnoreCancelled()) continue;
									o.getMethod().invoke(o.getObject(), e);
								}
								catch (InvocationTargetException x)
								{
									handleException(x.getCause(), e, false, true, o);
								}
							}
							catch (Throwable x)
							{
								handleException(x, e, false, true, o);
							}
				}
		}
		catch (Throwable x)
		{
			handleException(x, e, false, false, null);
		}
	}


	public void handleException(Throwable x, CommandEvent e, boolean wasHandled, boolean innerLoop, CDListenerObject cdlo)
	{
		try
		{
			if (x == null) return;
			if (wasHandled)
			{
				this.clog.printException(x);
				e.getSender().sendMessage(ChatColor.DARK_RED + "Error while processing a command" + (innerLoop ? ". Try to figure out the other invokes" : ""));
				this.clog.error("Error while processing a command. An Exception should has been logged" + (innerLoop ? ". Try to figure out the other invokes" : ""), this);
				this.handler.performErrorAction((CDPlugin) cdlo.getObject());
			}
			else if (x instanceof ClassCastException)
			{
				if (!x.getMessage().replaceAll("org.bukkit.[^ ]*(Command|Console)Sender cannot be cast to org.bukkit.entity.Player", "").equals(""))
				{
					handleException(x, e, true, innerLoop, cdlo);
					return;
				}
				this.clog.warn("Console trys to run a command that has to be run as a Player", this);
				Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "As Console you cannot run this command");
				return;
			}
			else if (x instanceof Handleable) ((Handleable) x).handle();
			else if (x instanceof CDCException) ((CDCException) x).handle(this.handler, e);
			else if (x instanceof CDGException) ((CDGException) x).handle();
			else handleException(x, e, true, innerLoop, cdlo);
		}
		catch (Throwable ex)
		{
			handleException(ex, e, false, innerLoop, cdlo);
		}
	}

	private void checkFullCommand(String fullCmd, CommandEvent e) throws CDNoPermissionException
	{
		if (checkFullCommand(fullCmd, e.getSender())) return;
		throw new CDNoPermissionException(fullCmd.split(" ")[2].equals("1"));
	}

	public boolean checkCommand(String parsedCmd, CommandSender sender)
	{
		List<String> l = this.commandList.get(parsedCmd.toLowerCase());
		if (l == null) return true;
		boolean allowed = true;
		for (String fullCmd : l)
			allowed &= checkFullCommand(fullCmd, sender);
		return allowed;
	}

	public boolean checkFullCommand(String fullCmd, CommandSender sender)
	{
		String[] parsedCmd = fullCmd.split(" ");
		if (sender instanceof ConsoleCommandSender || parsedCmd[1].equals(".") || sender.hasPermission(parsedCmd[1])) return true;
		this.clog.log(sender.getName() + " failed Permission-check for '" + fullCmd + "'", this);
		return false;
	}

	public void registerAReceive()
	{
		if (this.aggressiveChatListener != null) ProtocolLibrary.getProtocolManager().removePacketListener(this.aggressiveChatListener);
		this.aggressiveChatListener = new PacketAdapter(this.handler.getMain(), ListenerPriority.LOWEST, PacketType.Play.Client.CHAT)
		{
			@Override
			public void onPacketReceiving(PacketEvent e)
			{
				try
				{
					aggressiveReceive(e);
				}
				catch (Exception x)
				{}
			}
		};
		ProtocolLibrary.getProtocolManager().addPacketListener(this.aggressiveChatListener);
	}

	void aggressiveReceive(PacketEvent e)
	{
		String input = e.getPacket().getStrings().read(0);
		if (!input.startsWith("/")) return;
		if (input.equalsIgnoreCase("/cdapi aggressive"))
		{
			this.aggressiveCmdListener = !this.aggressiveCmdListener;
			this.clog.log("Switched to " + (this.aggressiveCmdListener ? "aggressive" : "normal") + " command receiving", this);
		}
		if (!this.aggressiveCmdListener) return;
		String strCmd = input.contains(" ") ? input.substring(1, input.indexOf(' ')) : input.substring(1);
		Command cmd = this.handler.getCommandRegister().commandMap.getCommand(strCmd);
		if (strCmd.equals("cdapi")) this.handler.getPlugin(CDAPICommand.class).onCommand(Var.stringToArr(input, 1), e.getPlayer());
		if (cmd == null || !(cmd instanceof CDCommand)) return;
		this.clog.log("Aggressivly received Command '" + input + "'", this);
		onCommand(cmd, Var.stringToArr(input, 1), e.getPlayer());
		e.setCancelled(true);
	}
}

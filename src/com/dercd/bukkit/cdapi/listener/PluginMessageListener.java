package com.dercd.bukkit.cdapi.listener;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.listener.objects.CDEventObject;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.wrapper.PluginMessage;

public class PluginMessageListener extends CDListener
{
	public PluginMessageListener(PluginHandler handler)
	{
		super(handler);
	}

	public void onPluginMessage(PluginMessage pm, Class<? extends CDPlugin> c, boolean lateRegistered)
	{
		String channel = pm.getChannel();
		if (!this.registered.containsKey(channel)) return;
		for (CDListenerObject o : this.registered.get(channel))
			if (o.getMethod().getClass().equals(c)) onPluginMessage(pm, (CDEventObject) o, lateRegistered);
	}

	private void onPluginMessage(PluginMessage pm, CDEventObject o, boolean lateRegistered)
	{
		try
		{
			if (lateRegistered != o.getLateRegistered()) return;
			if (pm.isCancelled() && !o.getIgnoreCancelled()) return;
			o.getMethod().invoke(o.getObject(), pm);
		}
		catch (Exception x)
		{
			handleException(x, false, o);
		}
	}

	public void onPluginMessage(PluginMessage pm, boolean lateRegistered)
	{
		String channel = pm.getChannel();
		if (!this.registered.containsKey(channel)) return;
		for (CDListenerObject o : this.registered.get(channel))
			onPluginMessage(pm, (CDEventObject) o, lateRegistered);
	}
}

package com.dercd.bukkit.cdapi.listener;

import org.bukkit.event.*;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.ActionEvent;
import com.dercd.bukkit.cdapi.listener.objects.CDEventObject;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;

public class EventListener extends CDListener implements Listener
{
	public EventListener(PluginHandler handler)
	{
		super(handler);
	}

	public void callEvent(Event e, Class<? extends CDPlugin> c)
	{
		if (!checkEvent(e)) return;
		String eventName = e.getClass().getName();
		if (!this.registered.containsKey(eventName)) return;
		for (CDListenerObject o : this.registered.get(eventName))
			if (o.getMethod().getClass().equals(c)) callEvent(e, (CDEventObject) o);
	}

	private void callEvent(Event e, CDEventObject o)
	{
		if (e instanceof ActionEvent) callEvent((ActionEvent) e, o);
		else
			try
			{
				if (!o.getIgnoreCancelled() && e instanceof Cancellable && ((Cancellable) e).isCancelled()) return;
				o.getMethod().invoke(o.getObject(), e);
			}
			catch (Throwable x)
			{
				handleException(x.getCause(), false, o);
			}
	}

	private void callEvent(ActionEvent e, CDEventObject o)
	{
		CDPlugin cdp = (CDPlugin) (o.getObject() instanceof CDPlugin ? o.getObject() : null);
		try
		{
			if (e instanceof Cancellable && ((Cancellable) e).isCancelled() && !o.getIgnoreCancelled()) return;
			o.getMethod().invoke(o.getObject(), e);
			if (cdp != null && !e.isSuccessSet(cdp)) e.setSuccess(cdp, true);
		}
		catch (Throwable x)
		{
			if (cdp != null)
			{
				if (!e.isSuccessSet(cdp)) e.setSuccess(cdp, false);
				this.handler.performErrorAction((CDPlugin) o.getObject());
				this.clog.printException(x);
			}
			else handleException(x.getCause(), false, o);
		}
	}

	public void onEvent(Event e)
	{
		if (!checkEvent(e)) return;
		String eventName = e.getClass().getName();
		if (!this.registered.containsKey(eventName)) return;
		for (CDListenerObject o : this.registered.get(eventName))
			callEvent(e, (CDEventObject) o);
	}

	public void onEvent(Event e, boolean lateRegistered, EventPriority ep)
	{
		if (!checkEvent(e)) return;
		String eventName = e.getClass().getName();
		if (!this.registered.containsKey(eventName)) return;
		CDEventObject cdeo;
		for (CDListenerObject o : this.registered.get(eventName))
			if ((cdeo = (CDEventObject) o).getBukkitPriority() == ep && cdeo.getLateRegistered() == lateRegistered) callEvent(e, cdeo);
	}

	private boolean checkEvent(Event e)
	{
		if (!(e instanceof ActionEvent)) return true;
		ActionEvent a = (ActionEvent) e;
		if (a.wasFired) return false;
		a.wasFired = true;
		return true;
	}
}

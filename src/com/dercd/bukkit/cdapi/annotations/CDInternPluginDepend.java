package com.dercd.bukkit.cdapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.dercd.bukkit.cdapi.CDPlugin;

/**
 * Annotation to show the (soft-)depends of CDPlugins of this CDPlugin
 * 
 * @author DerCD
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CDInternPluginDepend
{
	/**
	 * @return A list of classes extending from CDPlugin
	 *         <p>
	 *         this CDPlugin needs
	 */
	Class<? extends CDPlugin>[] depends();

	/**
	 * @return A list of names got by the CDPlugin.getName()-method
	 *         <p>
	 *         this CDPlugin needs for full functionality
	 */
	String[] softdepends();
}

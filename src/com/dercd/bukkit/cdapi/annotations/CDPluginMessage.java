package com.dercd.bukkit.cdapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to show which PluginMessage-channel should register to Bukkit
 * 
 * @author DerCD
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface CDPluginMessage
{
	/**
	 * @return A boolean which defines if this method should ignore the
	 *         cancel-state of a PluginMessage
	 * @default false
	 */
	boolean ignoreCancelled() default false;

	/**
	 * @return A list of Strings defining the channels to register
	 */
	String channel();

	/**
	 * @return A boolean which defines if this channels should register after
	 *         starting the Server
	 * @default false
	 */
	boolean lateRegister() default false;

	/**
	 * @return An int from 0 to 12000 specifying the intern priority.
	 *         <p>
	 *         0 is the lowest
	 * @default 10000
	 */
	int priority() default 10000;
}

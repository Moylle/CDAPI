package com.dercd.bukkit.cdapi.tools;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.minecraft.server.v1_9_R1.AttributeInstance;
import net.minecraft.server.v1_9_R1.AttributeMapBase;
import net.minecraft.server.v1_9_R1.Block;
import net.minecraft.server.v1_9_R1.BlockPosition;
import net.minecraft.server.v1_9_R1.EntityLiving;
import net.minecraft.server.v1_9_R1.EnumParticle;
import net.minecraft.server.v1_9_R1.GenericAttributes;
import net.minecraft.server.v1_9_R1.IAttribute;
import net.minecraft.server.v1_9_R1.IBlockData;
import net.minecraft.server.v1_9_R1.MinecraftServer;
import net.minecraft.server.v1_9_R1.NBTBase;
import net.minecraft.server.v1_9_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_9_R1.NBTTagByte;
import net.minecraft.server.v1_9_R1.NBTTagByteArray;
import net.minecraft.server.v1_9_R1.NBTTagCompound;
import net.minecraft.server.v1_9_R1.NBTTagDouble;
import net.minecraft.server.v1_9_R1.NBTTagFloat;
import net.minecraft.server.v1_9_R1.NBTTagInt;
import net.minecraft.server.v1_9_R1.NBTTagIntArray;
import net.minecraft.server.v1_9_R1.NBTTagList;
import net.minecraft.server.v1_9_R1.NBTTagLong;
import net.minecraft.server.v1_9_R1.NBTTagShort;
import net.minecraft.server.v1_9_R1.NBTTagString;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_9_R1.TileEntity;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_9_R1.CraftServer;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_9_R1.inventory.CraftItemStack;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.EnumWrappers.Particle;
import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.exceptions.CDUnsupportedPacketModifierException;
import com.dercd.bukkit.cdapi.listener.objects.CDListenerObject;
import com.dercd.bukkit.cdapi.tools.blockdata.BlockVariantData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sk89q.worldedit.bukkit.selections.Selection;

public abstract class Tools
{
	private Tools()
	{
	}
	
	public static class Var
	{
		private static Random r = new Random();
		private static Map<Class<? extends CDPlugin>, Map<String, Boolean>> savedBools = new HashMap<Class<? extends CDPlugin>, Map<String, Boolean>>();
		private static NBTTagCompound emptyItem;
		
		private Var()
		{
		}

		static
		{
			emptyItem = new NBTTagCompound();
			emptyItem.setShort("Damage", (short) 0);
			emptyItem.setShort("id", (short) 0);
			emptyItem.setByte("Count", (byte) 0);
		}


		public enum NBTType
		{
			INT, STRING, DOUBLE, FLOAT, BYTEARR, INTARR, COMPOUND, BYTE, SHORT, LONG, LIST, _STRINGARR
		}

		public static String arrToString(String[] input, String prefix, String suffix, int start, boolean cutBeg, boolean cutEnd)
		{
			return arrToString(input, prefix, suffix, start, input.length - start, cutBeg, cutEnd);
		}
		public static String arrToString(String[] input, String prefix, String suffix, int start, int length, boolean cutBeg, boolean cutEnd)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < length; i++)
				sb.append(prefix + input[i + start] + suffix);
			String s = sb.toString();
			if (cutBeg && s.length() >= prefix.length())
				s = s.substring(prefix.length());
			if (cutEnd && s.length() >= suffix.length())
				s = s.substring(0, s.length() - suffix.length());
			return s;
		}
		public static String arrToString(String[] input, int start)
		{
			return arrToString(input, " ", "", start, true, false);
		}
		public static String arrToString(String[] input, int start, int length)
		{
			return arrToString(input, " ", "", start, length, true, false);
		}

		public static String[] stringToArr(String input, int start)
		{
			return subArr(input.split(" "), start);
		}
		public static String[] subArr(String[] input, int start)
		{
			if (start >= input.length)
				return new String[0];
			String[] back = new String[input.length - start];
			for (int i = start; i < input.length; i++)
				back[i - start] = input[i];
			return back;
		}
		public static String[] replaceArr(String[] input, String search, String replacement)
		{
			String[] back = new String[input.length];
			for (int i = 0; i < input.length; i++)
				back[i] = (input[i].equals(search) ? replacement : input[i]);
			return back;
		}

		/**
		 * @deprecated Use
		 *             {@link #locationToString(Location,boolean,boolean,boolean)}
		 *             instead
		 */
		@Deprecated
		public static String parse(Location l, boolean pitch, boolean yaw, boolean blockCoords)
		{
			return locationToString(l, pitch, yaw, blockCoords);
		}

		public static String locationToString(Location l, boolean pitch, boolean yaw, boolean blockCoords)
		{
			StringBuilder sb = new StringBuilder();
			if (blockCoords)
				sb.append("x:" + l.getBlockX() + ", y:" + l.getBlockY() + ", z:" + l.getBlockZ());
			else
				sb.append("x:" + l.getX() + ", y:" + l.getY() + ", z:" + l.getZ());
			sb.append(", world:" + (l.getWorld() == null ? "null" : l.getWorld().getName()));
			if (pitch)
				sb.append(", pitch:" + l.getPitch());
			if (yaw)
				sb.append(", yaw:" + l.getYaw());
			return sb.toString();
		}

		public static String[] toArray(Collection<String> list)
		{
			Iterator<String> iter = list.iterator();
			String[] back = new String[list.size()];
			for (int i = 0; i < list.size(); i++)
				back[i] = iter.next();
			return back;
		}
		public static List<String> toList(String[] array)
		{
			List<String> back = new ArrayList<String>();
			for (String s : array)
				back.add(s);
			return back;
		}
		public static List<Integer> toList(int[] array)
		{
			List<Integer> back = new ArrayList<Integer>();
			for (int i : array)
				back.add(i);
			return back;
		}

		public static List<String> makeStringList(Collection<Class<? extends CDPlugin>> col)
		{
			List<String> back = new ArrayList<String>();
			for (Class<?> c : col)
				back.add(c.getSimpleName());
			return back;
		}

		public static List<String[]> makeStringArrList(List<String> list)
		{
			List<String[]> back = new ArrayList<String[]>();
			for (int i = 0; i < list.size(); i++)
				back.add(Var.stringToArr(list.get(i), 0));
			return back;
		}

		public static List<String> makeStringList(List<String[]> list)
		{
			List<String> back = new ArrayList<String>();
			if (list != null)
				for (String[] sArr : list)
				back.add(Var.arrToString(sArr, 0));
			return back;
		}

		public static Map<String[], Boolean> combineLists(List<String[]> first, List<String[]> second)
		{
			Map<String[], Boolean> back = new HashMap<String[], Boolean>();
			if (first != null)
				for (String[] o : first)
				back.put(o, true);
			if (second != null)
				for (String[] o : second)
				back.put(o, false);
			return back;
		}

		public static <C> List<C> combineSets(Set<C> first, Set<C> second)
		{
			List<C> back = new ArrayList<C>(first);
			if (second != null)
				back.addAll(second);
			return back;
		}

		/**
		 * @deprecated Use {@link #createNBTList(List<? extends NBTBase>)}
		 *             instead
		 */
		@SuppressWarnings("javadoc")
		@Deprecated
		public static NBTTagList convertList(List<? extends NBTBase> list)
		{
			return createNBTList(list);
		}

		public static NBTTagList createNBTList(List<? extends NBTBase> list)
		{
			NBTTagList back = new NBTTagList();
			for (NBTBase b : list)
				back.add(b);
			return back;
		}

		/**
		 * @deprecated Use {@link #addBeginningToEntrys(List<String>,String)}
		 *             instead
		 */
		@SuppressWarnings("javadoc")
		@Deprecated
		public static void addBeg(List<String> list, String beg)
		{
			addBeginningToEntrys(list, beg);
		}

		public static void addBeginningToEntrys(List<String> list, String beg)
		{
			for (int i = 0; i < list.size(); i++)
				list.set(i, beg + list.get(i));
		}

		public static boolean ifInt(String input)
		{
			try
			{
				Integer.valueOf(input);
				return true;
			}
			catch (Exception x)
			{
				return false;
			}
		}
		public static boolean ifShort(String input)
		{
			try
			{
				Short.valueOf(input);
				return true;
			}
			catch (Exception x)
			{
				return false;
			}
		}
		public static boolean ifLong(String input)
		{
			try
			{
				Long.valueOf(input);
				return true;
			}
			catch (Exception x)
			{
				return false;
			}
		}
		public static boolean ifDouble(String input)
		{
			try
			{
				Double.valueOf(input);
				return true;
			}
			catch (Exception x)
			{
				return false;
			}
		}
		public static boolean ifFloat(String input)
		{
			try
			{
				Float.valueOf(input);
				return true;
			}
			catch (Exception x)
			{
				return false;
			}
		}
		public static boolean ifBool(String input)
		{
			return (input.equalsIgnoreCase("true") || input.equalsIgnoreCase("false"));
		}

		public static Inventory cloneInventory(Inventory i)
		{
			Inventory cloned = Bukkit.createInventory(i.getHolder(), i.getSize(), i.getName());
			cloned.setContents(i.getContents());
			return cloned;
		}

		@SuppressWarnings("unchecked")
		public static <C> C[] combineArray(C[] a1, C[] a2)
		{
			Class<?> clazz = findClass(a1);
			if (clazz == null)
			{
				findClass(a2);
				if (clazz == null)
					return null;
			}
			C[] back = (C[]) Array.newInstance(clazz, a1.length + a2.length);
			for (int i = 0; i < a1.length; i++)
				back[i] = a1[i];
			for (int i = a1.length; i < back.length; i++)
				back[i] = a2[i - a1.length];
			return back;
		}
		@SuppressWarnings("unchecked")
		public static <C> C[] combineArray(C[] a1, C[] a2, Class<C> clazz)
		{
			C[] back = (C[]) Array.newInstance(clazz, a1.length + a2.length);
			for (int i = 0; i < a1.length; i++)
				back[i] = a1[i];
			for (int i = a1.length; i < back.length; i++)
				back[i] = a2[i - a1.length];
			return back;
		}

		public static List<Location> getLocations(Selection s)
		{
			return getLocations(s.getMinimumPoint(), s.getMaximumPoint());
		}
		public static List<Location> getLocations(Location l1, Location l2)
		{
			List<Location> back = new ArrayList<Location>();
			int x1 = l1.getBlockX(), y1 = l1.getBlockY(), z1 = l1.getBlockZ(), x2 = l2.getBlockX(), y2 = l2.getBlockY(), z2 = l2.getBlockZ();
			World w = l1.getWorld();
			for (int ix = x1; ix <= x2; ix++)
				for (int iy = y1; iy <= y2; iy++)
					for (int iz = z1; iz <= z2; iz++)
						back.add(new Location(w, ix, iy, iz));
			return back;
		}

		public static <C> List<C> reverseList(List<C> input)
		{
			List<C> back = new ArrayList<C>();
			for (int i = input.size() - 1; i >= 0; i--)
				back.add(input.get(i));
			return back;
		}

		public static void sortCalls(Map<String, List<CDListenerObject>> map)
		{
			List<CDListenerObject> val;
			List<CDListenerObject> newval;
			for (String str : map.keySet())
			{
				val = map.get(str);
				newval = new ArrayList<CDListenerObject>();
				for (int p = 0; !val.isEmpty() && p <= 12000; p++)
					for (CDListenerObject o : new ArrayList<CDListenerObject>(val))
						if (o.getPriority() == p)
						{
							val.remove(o);
							newval.add(o);
						}
				map.put(str, Var.reverseList(newval));
			}
		}

		public static <C> Collection<C> cloneCollection(Collection<C> collection)
		{
			Collection<C> back = new ArrayList<C>();
			for (C cmd : collection)
				back.add(cmd);
			return back;
		}

		public static Location[] getBox(String[] args, int start, World w)
		{
			Double[] points = new Double[] { null, null, null, null, null, null };
			for (int i = 0; i < 6; i++)
				points[i] = Double.valueOf(args[i + start]);
			return getBox(points, w);
		}
		public static Location[] getBox(Double[] points, World w)
		{
			return new Location[] { new Location(w, points[0], points[1], points[2]), new Location(w, points[3], points[4], points[5]) };
		}
		public static Location[] getBox(Location middle, int radius)
		{
			Location[] back = new Location[2];
			double x = middle.getX(), y = middle.getY(), z = middle.getZ();
			back[0] = new Location(middle.getWorld(), x - radius, y - radius, z - radius);
			back[1] = new Location(middle.getWorld(), x + radius, y + radius, z + radius);
			return back;
		}

		/**
		 * @deprecated Use {@link #getPointFromStringArr(String[],int,World)}
		 *             instead
		 */
		@Deprecated
		public static Location getPoint(String[] args, int start, World w)
		{
			return getPointFromStringArr(args, start, w);
		}

		public static Location getPointFromStringArr(String[] args, int start, World w)
		{
			double[] points = new double[3];
			for (int i = 0; i < 3; i++)
				points[i] = Double.valueOf(args[i + start]);
			return new Location(w, points[0], points[1], points[2]);
		}

		public static List<Location> getLocations(Location[] points)
		{
			List<Location> back = new ArrayList<Location>();
			int[] coords = new int[] { points[0].getBlockX(), points[1].getBlockX(), points[0].getBlockY(), points[1].getBlockY(), points[0].getBlockZ(), points[1].getBlockZ() };
			sortNumberPairs(coords);
			World w = points[0].getWorld();
			for (int ix = coords[0]; ix <= coords[1]; ix++)
				for (int iy = coords[2]; iy <= coords[3]; iy++)
					for (int iz = coords[4]; iz <= coords[5]; iz++)
						back.add(new Location(w, ix, iy, iz));
			return back;
		}

		public static void sortNumberPairs(int[] n)
		{
			int[] temp = new int[2];
			for (int i = 0; i + 1 < n.length; i += 2)
			{
				temp[0] = n[i];
				temp[1] = n[i + 1];
				sortNumberPair(temp);
				n[i] = temp[0];
				n[i + 1] = temp[1];
			}
		}

		public static void sortNumberPair(int[] n)
		{
			int temp;
			if (n[0] > n[1])
			{
				temp = n[0];
				n[0] = n[1];
				n[1] = temp;
			}
		}

		public static boolean isPlayer(Object o)
		{
			return (o instanceof Player);
		}

		/**
		 * @deprecated Use {@link #validatePlayer(Object)} instead
		 */
		@Deprecated
		public static boolean checkPlayer(Object o)
		{
			return validatePlayer(o);
		}

		public static boolean validatePlayer(Object o)
		{
			return o != null && isPlayer(o);
		}

		/**
		 * @deprecated Use {@link #broadcastMessages(List<String>,Player)}
		 *             instead
		 */
		@Deprecated
		@SuppressWarnings("javadoc")
		public static void showMessages(List<String> messages, Player p)
		{
			broadcastMessages(messages, p);
		}

		public static void broadcastMessages(List<String> messages, Player p)
		{
			for (String line : messages)
				p.sendMessage(line);
		}

		public static Location readCoords(NBTTagCompound cpd)
		{
			try
			{
				if (!cpd.hasKey("x") || !cpd.hasKey("y") || !cpd.hasKey("z") || !cpd.hasKey("world"))
					return null;
				if (cpd.get("x") instanceof NBTTagInt)
					return new Location(Bukkit.getWorld(cpd.getString("world")), cpd.getInt("x"), cpd.getInt("y"), cpd.getInt("z"));
				return new Location(Bukkit.getWorld(cpd.getString("world")), cpd.getDouble("x"), cpd.getDouble("y"), cpd.getDouble("z"), cpd.getFloat("yaw"), cpd.getFloat("pitch"));
			}
			catch (Exception x)
			{
				return null;
			}
		}
		public static Location readCoords(NBTTagCompound cpd, World w)
		{
			if (cpd.get("x") instanceof NBTTagInt)
				return new Location(w, cpd.getInt("x"), cpd.getInt("y"), cpd.getInt("z"));
			return new Location(w, cpd.getDouble("x"), cpd.getDouble("y"), cpd.getDouble("z"), cpd.getFloat("yaw"), cpd.getFloat("pitch"));
		}
		public static Vector readVector(NBTTagCompound cpd)
		{
			try
			{
				if (!cpd.hasKey("x") || !cpd.hasKey("y") || !cpd.hasKey("z"))
					return null;
				if (cpd.get("x") instanceof NBTTagInt)
					return new Vector(cpd.getInt("x"), cpd.getInt("y"), cpd.getInt("z"));
				return new Vector(cpd.getDouble("x"), cpd.getDouble("y"), cpd.getDouble("z"));
			}
			catch (Exception x)
			{
				return null;
			}
		}
		private static void readModifier(NBTBase o, NBTType t, PacketContainer pc, int id)
		{
			switch (t)
			{
				case INT:
					pc.getModifier().write(id, ((NBTTagInt) o).d());
					break;
				case STRING:
					pc.getModifier().write(id, ((NBTTagString) o).a_());
					break;
				case DOUBLE:
					pc.getModifier().write(id, ((NBTTagDouble) o).g());
					break;
				case FLOAT:
					pc.getModifier().write(id, ((NBTTagFloat) o).h());
					break;
				case BYTEARR:
					pc.getModifier().write(id, ((NBTTagByteArray) o).c());
					break;
				case INTARR:
					pc.getModifier().write(id, ((NBTTagIntArray) o).c());
					break;
				case COMPOUND:
					pc.getModifier().write(id, o);
					break;
				case BYTE:
					pc.getModifier().write(id, ((NBTTagByte) o).f());
					break;
				case LONG:
					pc.getModifier().write(id, ((NBTTagLong) o).c());
					break;
				case SHORT:
					pc.getModifier().write(id, ((NBTTagShort) o).e());
					break;
				case LIST:
					pc.getModifier().write(id, o);
				case _STRINGARR:
					NBTTagList strList = (NBTTagList) o;
					String[] strings = new String[strList.size()];
					for (int i = 0; i < strList.size(); i++)
						strings[i] = strList.getString(i);
					pc.getModifier().write(id, strings);
					break;
			}
		}

		public static void writeCoords(NBTTagCompound cpd, Location l)
		{
			cpd.set("x", new NBTTagInt(l.getBlockX()));
			cpd.set("y", new NBTTagInt(l.getBlockY()));
			cpd.set("z", new NBTTagInt(l.getBlockZ()));
			cpd.set("world", new NBTTagString(l.getWorld().getName()));
		}
		public static NBTTagCompound writeCoords(Location l)
		{
			NBTTagCompound cpd = new NBTTagCompound();
			writeCoords(cpd, l);
			return cpd;
		}
		public static void writeCoordsExact(NBTTagCompound cpd, Location l)
		{
			cpd.setDouble("x", l.getX());
			cpd.setDouble("y", l.getY());
			cpd.setDouble("z", l.getZ());
			cpd.setFloat("pitch", l.getPitch());
			cpd.setFloat("yaw", l.getYaw());
			cpd.setString("world", l.getWorld().getName());
		}
		public static NBTTagCompound writeCoordsExact(Location l)
		{
			NBTTagCompound cpd = new NBTTagCompound();
			writeCoordsExact(cpd, l);
			return cpd;
		}
		public static void writeVector(NBTTagCompound cpd, Vector v)
		{
			cpd.set("x", new NBTTagInt(v.getBlockX()));
			cpd.set("y", new NBTTagInt(v.getBlockY()));
			cpd.set("z", new NBTTagInt(v.getBlockZ()));
		}
		public static NBTTagCompound writeVector(Vector v)
		{
			NBTTagCompound cpd = new NBTTagCompound();
			writeVector(cpd, v);
			return cpd;
		}
		public static void writeVectorExact(NBTTagCompound cpd, Vector v)
		{
			cpd.setDouble("x", v.getX());
			cpd.setDouble("y", v.getY());
			cpd.setDouble("z", v.getZ());
		}
		public static NBTTagCompound writeVectorExact(Vector v)
		{
			NBTTagCompound cpd = new NBTTagCompound();
			writeVectorExact(cpd, v);
			return cpd;
		}
		private static void writeModifier(Object o, NBTType t, NBTTagCompound cpd)
		{
			switch (t)
			{
				case INT:
					cpd.set("Type", new NBTTagString("INT"));
					cpd.set("Value", new NBTTagInt((int) o));
					break;
				case STRING:
					cpd.set("Type", new NBTTagString("STRING"));
					cpd.set("Value", new NBTTagString((String) o));
					break;
				case DOUBLE:
					cpd.set("Type", new NBTTagString("DOUBLE"));
					cpd.set("Value", new NBTTagDouble((double) o));
					break;
				case FLOAT:
					cpd.set("Type", new NBTTagString("FLOAT"));
					cpd.set("Value", new NBTTagFloat((float) o));
					break;
				case BYTEARR:
					cpd.set("Type", new NBTTagString("BYTEARR"));
					cpd.set("Value", new NBTTagByteArray((byte[]) o));
					break;
				case INTARR:
					cpd.set("Type", new NBTTagString("INTARR"));
					cpd.set("Value", new NBTTagIntArray((int[]) o));
					break;
				case COMPOUND:
					cpd.set("Type", new NBTTagString("COMPOUND"));
					cpd.set("Value", (NBTTagCompound) o);
					break;
				case BYTE:
					cpd.set("Type", new NBTTagString("BYTE"));
					cpd.set("Value", new NBTTagByte((byte) o));
					break;
				case LONG:
					cpd.set("Type", new NBTTagString("LONG"));
					cpd.set("Value", new NBTTagLong((long) o));
					break;
				case SHORT:
					cpd.set("Type", new NBTTagString("SHORT"));
					cpd.set("Value", new NBTTagShort((short) o));
					break;
				case LIST:
					cpd.set("Type", new NBTTagString("LIST"));
					cpd.set("Value", (NBTTagList) o);
				case _STRINGARR:
					cpd.set("Type", new NBTTagString("_STRINGARR"));
					NBTTagList strList = new NBTTagList();
					for (String s : (String[]) o)
						strList.add(new NBTTagString(s));
					cpd.set("Value", strList);
					break;
				default:
					break;
			}
		}

		public static PacketType getPacketType(String type, boolean serverSide)
		{
			for (PacketType packetType : PacketType.values())
				if (packetType.isServer() != serverSide)
					continue;
				else if (packetType.name().equalsIgnoreCase(type))
					return packetType;
			return null;
		}

		private static NBTType getNBTType(Object o)
		{
			if (o instanceof Integer)
				return NBTType.INT;
			else if (o instanceof String)
				return NBTType.STRING;
			else if (o instanceof Double)
				return NBTType.DOUBLE;
			else if (o instanceof Float)
				return NBTType.FLOAT;
			else if (o instanceof byte[])
				return NBTType.BYTEARR;
			else if (o instanceof int[])
				return NBTType.INTARR;
			else if (o instanceof NBTTagCompound)
				return NBTType.COMPOUND;
			else if (o instanceof Byte)
				return NBTType.BYTE;
			else if (o instanceof Short)
				return NBTType.SHORT;
			else if (o instanceof Long)
				return NBTType.LONG;
			else if (o instanceof NBTTagList)
				return NBTType.LIST;
			else if (o instanceof String[])
				return NBTType._STRINGARR;
			else
				return null;
		}
		private static NBTType getNBTType(NBTTagCompound cpd)
		{
			return NBTType.valueOf(cpd.getString("Type").toUpperCase());
		}

		/**
		 * @deprecated Use {@link #createListFromNBT(NBTTagList)} instead
		 */
		@Deprecated
		public static List<String> convertNBTList(NBTTagList nbtList)
		{
			return createListFromNBT(nbtList);
		}

		public static List<String> createListFromNBT(NBTTagList nbtList)
		{
			List<String> back = new ArrayList<String>();
			for (int i = 0; i < nbtList.size(); i++)
				back.add(nbtList.getString(i));
			return back;
		}

		/**
		 * @deprecated Use {@link #createNBTListFromStrings(List<String>)}
		 *             instead
		 */
		@SuppressWarnings("javadoc")
		@Deprecated
		public static NBTTagList convertNBTList(List<String> list)
		{
			return createNBTListFromStrings(list);
		}

		public static NBTTagList createNBTListFromStrings(List<String> list)
		{
			NBTTagList back = new NBTTagList();
			for (String s : list)
				back.add(new NBTTagString(s));
			return back;
		}

		public static NBTTagCompound writePacket(PacketContainer pc) throws CDUnsupportedPacketModifierException
		{
			NBTTagCompound base = new NBTTagCompound();
			if (pc == null)
				return base;
			base.set("Type", new NBTTagString(pc.getType().name()));
			base.set("ServerSide", new NBTTagByte((byte) (pc.getType().isServer() ? 1 : 0)));
			NBTTagList modifier = new NBTTagList();
			NBTType type;
			NBTTagCompound modCpd;
			StructureModifier<Object> objects = pc.getModifier();
			int size = objects.getValues().size();
			Object o;
			for (int i = 0; i < size; i++)
			{
				o = objects.read(i);
				type = getNBTType(o);
				if (type == null)
					throw new CDUnsupportedPacketModifierException(o);
				modCpd = new NBTTagCompound();
				modCpd.set("id", new NBTTagInt(i));
				writeModifier(o, type, modCpd);
				modifier.add(modCpd);
			}
			base.set("Data", modifier);
			return base;
		}
		public static PacketContainer readPacket(NBTTagCompound cpd)
		{
			PacketType packetType = getPacketType(cpd.getString("Type"), cpd.getByte("ServerSide") == (byte) 1);
			if (packetType == null)
				return null;
			PacketContainer pc = new PacketContainer(packetType);
			NBTTagList data = (NBTTagList) cpd.get("Data");
			NBTTagCompound modCpd;
			NBTBase value;
			for (int i = 0; i < data.size(); i++)
			{
				modCpd = data.get(i);
				value = modCpd.get("Value");
				readModifier(value, getNBTType(modCpd), pc, modCpd.getInt("id"));
			}
			return pc;
		}

		public static String cut(String input, int maxLength)
		{
			if (input.length() <= maxLength)
				return input;
			return input.substring(0, maxLength);
		}
		public static String cut(String input, int start, int length)
		{
			if (input.length() <= length)
				return input.substring(start);
			return input.substring(start, start + length);
		}

		public static String cutEnd(String input, int cut)
		{
			if (input.length() <= cut)
				return "";
			return input.substring(0, input.length() - cut);
		}

		public static String removeUselessPath(String path)
		{
			return path.replaceAll("\\.[\\\\/]", "");
		}

		/**
		 * @deprecated Use {@link #addEndingToAbsulutePath(String)} instead
		 */
		@Deprecated
		public static String addEnding(String path)
		{
			return addEndingToAbsulutePath(path);
		}

		public static String addEndingToAbsulutePath(String path)
		{
			String end = (path.substring(1, 3).equals(":\\") ? "\\" : "/");
			if (!path.endsWith(end))
				path += end;
			return path;
		}

		public static String getExclamation(ChatColor c)
		{
			return "" + ChatColor.RESET + c + ChatColor.BOLD + ChatColor.MAGIC + "!!!" + ChatColor.RESET + " ";
		}

		public static String[] toHex(byte[] i)
		{
			String[] back = new String[i.length];
			for (int j = 0; j < i.length; j++)
				back[j] = toHex(i[j]);
			return back;
		}
		public static String toHex(byte i)
		{
			if (i < 0 || i > 16)
				return "0";
			if (i < 10)
				return "" + i;
			switch (i)
			{
				case 10:
					return "a";
				case 11:
					return "b";
				case 12:
					return "c";
				case 13:
					return "d";
				case 14:
					return "e";
				case 15:
					return "f";
				case 16:
					return "n";
			}
			return "0";
		}

		public static NBTTagCompound writeItem(ItemStack i)
		{
			NBTTagCompound back = new NBTTagCompound();
			writeItem(i, back);
			return back;
		}
		public static void writeItem(ItemStack i, NBTTagCompound cpd)
		{
			CraftItemStack.asNMSCopy(i).save(cpd);
		}

		/**
		 * @deprecated Use {@link #writeInventory(ItemStack[])} instead
		 */
		@Deprecated
		public static NBTTagList writeInv(ItemStack[] contents)
		{
			return writeInventory(contents);
		}

		public static NBTTagList writeInventory(ItemStack[] contents)
		{
			NBTTagList inventoryTag = new NBTTagList();
			NBTTagCompound item;
			ItemStack currentItem;
			for (int i = 0; i < contents.length; i++)
			{
				currentItem = contents[i];
				if (currentItem != null && currentItem.getType() != Material.AIR)
				{
					item = writeItem(currentItem);
					item.setByte("Slot", (byte) i);
					inventoryTag.add(CraftItemStack.asNMSCopy(currentItem).save(item));
				}
				else if (i + 1 == contents.length)
				{
					NBTTagCompound emptyItem = (NBTTagCompound) Var.emptyItem.clone();
					emptyItem.setByte("Slot", (byte) i);
					inventoryTag.add(emptyItem);
				}
			}
			return inventoryTag;
		}

		public static ItemStack readItem(NBTTagCompound cpd)
		{
			if ("minecraft:air".equals(cpd.getString("id")))
				return null;
			return CraftItemStack.asCraftMirror(net.minecraft.server.v1_9_R1.ItemStack.createStack(cpd));
		}

		/**
		 * @deprecated Use {@link #readInventory(NBTTagList)} instead
		 */
		@Deprecated
		public static ItemStack[] readInv(NBTTagList list)
		{
			return readInventory(list);
		}

		public static ItemStack[] readInventory(NBTTagList list)
		{
			NBTTagCompound item;
			Map<Integer, ItemStack> map = new HashMap<Integer, ItemStack>();
			for (int i = 0; i < list.size(); ++i)
			{
				item = list.get(i);
				int slot = item.getByte("Slot") & 0xFF;
				map.put(slot, readItem(item));
			}
			return makeInv(map);
		}

		public static ItemStack[] makeInv(Map<Integer, ItemStack> map)
		{
			int highest = 8;
			for (Integer act : map.keySet())
				highest = Math.max(highest, act);
			while (++highest % 9 != 0);
			ItemStack[] back = new ItemStack[highest];
			for (Integer act : map.keySet())
				back[act] = map.get(act);
			return back;
		}

		public static void convertToShowableInv(ItemStack[] inv)
		{
			if (inv.length != 36)
				throw new IllegalArgumentException();
			swapParts(inv, 0, 8, 27, 35);
			swapParts(inv, 9, 17, 27, 35);
			swapParts(inv, 18, 26, 27, 35);
		}

		@SuppressWarnings("unchecked")
		public static <C> void swapParts(C[] array, int from1, int to1, int from2, int to2)
		{
			Class<C> clazz = findClass(array);
			if (clazz == null)
				return;
			C[] one = (C[]) Array.newInstance(clazz, to1 - from1 + 1);
			C[] two = (C[]) Array.newInstance(clazz, to2 - from2 + 1);
			if (to1 - from1 != to2 - from2 || from1 > to1 || from2 > to2)
				throw new IllegalArgumentException();
			for (int i = from1; i <= to1; i++)
				one[i - from1] = array[i];
			for (int i = from2; i <= to2; i++)
				two[i - from2] = array[i];
			for (int i = 0; i < two.length; i++)
				array[i + from1] = two[i];
			for (int i = 0; i < one.length; i++)
				array[i + from2] = one[i];
		}

		@SuppressWarnings("unchecked")
		public static <C> Class<C> findClass(C[] array)
		{
			if (array == null)
				return null;
			Class<C> c = null;
			for (C o : array)
				if (o != null)
				{
					c = (Class<C>) o.getClass();
					break;
				}
			return c;
		}

		public static Location getBlockLoc(Location l)
		{
			return new Location(l.getWorld(), l.getBlockX(), l.getBlockY(), l.getBlockZ());
		}

		public static String packTime(long seconds)
		{
			String s = "";
			long i;
			i = seconds / 31536000;
			if (i != 0)
				s += i + "y";
			seconds = seconds % 31536000;
			i = seconds / 604800;
			if (i != 0)
				s += i + "w";
			seconds = seconds % 604800;
			i = seconds / 86400;
			if (i != 0)
				s += i + "d";
			seconds = seconds % 86400;
			i = seconds / 3600;
			if (i != 0)
				s += i + "h";
			seconds = seconds % 3600;
			i = seconds / 60;
			if (i != 0)
				s += i + "m";
			seconds = seconds % 60;
			if (seconds != 0)
				s += seconds + "s";
			return s;
		}

		public static int unpackTime(String s)
		{
			int back = 0;
			List<String> part;
			for (String p : splitString(s, "[0-9]+[ywdhms]"))
			{
				part = splitString(p, "([0-9]+)|([ywdhms])");
				switch (part.get(1))
				{
					case "y":
						back += Integer.valueOf(part.get(0)) * 31536000;
						break;
					case "w":
						back += Integer.valueOf(part.get(0)) * 604800;
						break;
					case "d":
						back += Integer.valueOf(part.get(0)) * 86400;
						break;
					case "h":
						back += Integer.valueOf(part.get(0)) * 3600;
						break;
					case "m":
						back += Integer.valueOf(part.get(0)) * 60;
						break;
					case "s":
						back += Integer.valueOf(part.get(0));
						break;
				}
			}
			return back;
		}

		private static List<String> splitString(String input, String regex)
		{
			Pattern p = Pattern.compile(regex);
			List<String> back = new ArrayList<String>();
			Matcher m = p.matcher(input);
			while (m.find())
				back.add(input.substring(m.start(), m.end()));
			return back;
		}

		/**
		 * @deprecated Use {@link #deepCloneTileEntity(TileEntity)} instead
		 */
		@Deprecated
		public static TileEntity clone(TileEntity o)
		{
			return deepCloneTileEntity(o);
		}

		public static TileEntity deepCloneTileEntity(TileEntity o)
		{
			if (o == null)
				return null;
			NBTTagCompound cpd = new NBTTagCompound();
			o.save(cpd);
			return TileEntity.a(Minecraft.mcServer, cpd);
		}

		public static boolean compare(List<?> l1, List<?> l2)
		{
			if (l1.size() != l2.size())
				return false;
			for (int i = 0; i < l1.size(); i++)
				if (!l1.get(i).equals(l2.get(i)))
					return false;
			return true;
		}

		public static Location getMiddle(Location l)
		{
			return new Location(l.getWorld(), l.getBlockX() + 0.5, l.getY(), l.getBlockZ() + 0.5, l.getYaw(), l.getPitch());
		}

		public static boolean hasMoved(Location l1, Location l2)
		{
			return !(l1.getBlockX() == l2.getBlockX() && l1.getBlockZ() == l2.getBlockZ());
		}

		public static List<Byte> toList(byte[] input)
		{
			List<Byte> back = new ArrayList<Byte>();
			for (byte b : input)
				back.add(b);
			return back;
		}

		public static String getPriority(int i)
		{
			if (i < 2000)
				return "MONITOR";
			if (i < 4000)
				return "HIGHEST";
			if (i < 6000)
				return "HIGH";
			if (i < 8000)
				return "NORMAL";
			if (i < 10000)
				return "LOW";
			return "LOWEST";
		}

		public static boolean hasKey(NBTTagCompound cpd, String name, NBTType type)
		{
			NBTType t = null;
			NBTBase base = cpd.get(name);
			if (base == null)
				return false;
			if (base instanceof NBTTagString)
				t = NBTType.STRING;
			else if (base instanceof NBTTagInt)
				t = NBTType.INT;
			else if (base instanceof NBTTagShort)
				t = NBTType.SHORT;
			else if (base instanceof NBTTagDouble)
				t = NBTType.DOUBLE;
			else if (base instanceof NBTTagFloat)
				t = NBTType.FLOAT;
			else if (base instanceof NBTTagByte)
				t = NBTType.BYTE;
			else if (base instanceof NBTTagByteArray)
				t = NBTType.BYTEARR;
			else if (base instanceof NBTTagIntArray)
				t = NBTType.INTARR;
			else if (base instanceof NBTTagList)
				t = NBTType.LIST;
			else if (base instanceof NBTTagCompound)
				t = NBTType.COMPOUND;
			else if (base instanceof NBTTagLong)
				t = NBTType.LONG;
			return t == type;
		}

		public static <C> List<C> removeDouble(List<C> input)
		{
			List<C> back = new ArrayList<C>();
			for (C s : input)
				if (!back.contains(s))
					back.add(s);
			return back;
		}

		public static <C> List<C> makeList(Map<Integer, C> map)
		{
			List<C> back = new ArrayList<C>();
			int c = -1;
			while (!map.isEmpty())
			{
				c++;
				for (Integer i : new HashSet<Integer>(map.keySet()))
					if (i == c)
					{
						back.add(map.remove(i));
						continue;
					}
			}
			return back;
		}

		public static Iterator<Integer> calcConsistent(int available, int toFill)
		{
			List<Integer> back = new ArrayList<Integer>();
			if (available <= 0)
				return null;
			while (available < toFill)
			{
				for (int i = 0; i < available; i++)
					back.add(i);
				toFill -= available;
			}
			double d = available / toFill;
			for (int i = 0; i < toFill; i++)
				back.add((int) (i * d + 0.5));
			return back.iterator();
		}

		public static <C> boolean containsOne(Collection<C> c, Collection<C> toFind)
		{
			for (C o : toFind)
				if (c.contains(o))
					return true;
			return false;
		}

		public static <C> C getFirst(Collection<? extends C> c, Collection<? extends C> toFind)
		{
			for (C o : toFind)
				if (c.contains(o))
					return o;
			return null;
		}

		public static int newPositiveRandomInt()
		{
			return Math.abs(r.nextInt());
		}

		public static String escapeRegex(String input)
		{
			return input.replace("\\", "\\\\").replace("(", "\\(").replace(")", "\\)").replace("[", "\\[").replace("]", "\\]").replace("+", "\\+").replace("?", "\\?").replace("*", "\\*");
		}

		public static String getPackageName(String className)
		{
			return className.substring(0, className.lastIndexOf('.'));
		}

		public static <K, V> Map<K, V> toMap(List<K> list, V value)
		{
			Map<K, V> back = new HashMap<K, V>();
			for (K k : list)
				back.put(k, value);
			return back;
		}

		public static boolean containsStringCaseInsensitive(Collection<String> col, String s)
		{
			return getStringCaseInsensitive(col, s) != null;
		}

		public static String getStringCaseInsensitive(Collection<String> col, String s)
		{
			for (String str : col)
				if (str.equalsIgnoreCase(s))
					return str;
			return null;
		}

		public static <C> void removeObjects(Collection<C> col, Collection<C> toRemove)
		{
			for (C c : toRemove)
				col.remove(c);
		}

		public static boolean createBool(Class<? extends CDPlugin> clazz, String name)
		{
			HashMap<String, Boolean> pluginBools = (HashMap<String, Boolean>) savedBools.get(clazz);
			if (pluginBools == null)
				savedBools.put(clazz, (pluginBools = new HashMap<String, Boolean>()));
			Boolean back = pluginBools.put(name, true);
			return back == null ? false : back;
		}

		public static boolean isDoBool(Class<? extends CDPlugin> clazz, String name)
		{
			Map<String, Boolean> pluginBools = savedBools.get(clazz);
			if (pluginBools == null)
				return false;
			Boolean b = pluginBools.get(name);
			if (b)
				pluginBools.put(name, false);
			return (b == null ? false : b);
		}

		public static boolean isBool(Class<? extends CDPlugin> clazz, String name)
		{
			Map<String, Boolean> pluginBools = savedBools.get(clazz);
			if (pluginBools == null)
				return false;
			Boolean b = pluginBools.get(name);
			return (b == null ? false : b);
		}

		public static String expandString(String s, int expand)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < expand; i++)
				sb.append(s);
			return sb.toString();
		}

		public static Collection<Player> substractUUIDs(Collection<? extends Player> players, Collection<UUID> uuids)
		{
			Map<UUID, Player> map = new HashMap<UUID, Player>();
			for (Player p : players)
				map.put(p.getUniqueId(), p);
			for (UUID u : uuids)
				map.remove(u);
			return map.values();
		}

		public static List<Location> checkSelection(Selection s, int amount)
		{
			return checkSelection(s, amount, amount);
		}
		public static List<Location> checkSelection(Selection s, int min, int max)
		{
			List<Location> l = getLocations(s);
			return l.size() >= min && l.size() <= max ? l : null;
		}

		public static boolean ifUUID(String s)
		{
			return isUUID(s);
		}

		public static <C> void addToArray(C[] array, C[] toAdd)
		{
			int max = Math.min(array.length, toAdd.length);
			for (int i = 0; i < max; i++)
				array[i] = toAdd[i];
		}

		public static long strToTime(String s)
		{
			Calendar c = Calendar.getInstance();
			List<String> part;
			for (String p : splitString(s, "[0-9]+[ywdhms]"))
			{
				part = splitString(p, "([0-9]+)|([ywdhms])");
				switch (part.get(1))
				{
					case "y":
						c.set(Calendar.YEAR, Integer.valueOf(part.get(0)));
						break;
					case "M":
						c.set(Calendar.MONTH, Integer.valueOf(part.get(0)));
						break;
					case "d":
						c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(part.get(0)));
						break;
					case "h":
						c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(part.get(0)));
						break;
					case "m":
						c.set(Calendar.MINUTE, Integer.valueOf(part.get(0)));
						break;
					case "s":
						c.set(Calendar.SECOND, Integer.valueOf(part.get(0)));
						break;
				}
			}
			return c.getTimeInMillis() / 1000;
		}

		public static boolean isUUID(String s)
		{
			try
			{
				UUID.fromString(s);
				return true;
			}
			catch (Exception x)
			{
				return false;
			}
		}

		public static NBTTagCompound writePotionEffect(PotionEffect pe)
		{
			NBTTagCompound base = new NBTTagCompound();
			writePotionEffect(pe, base);
			return base;
		}

		public static void writePotionEffect(PotionEffect pe, NBTTagCompound cpd)
		{
			cpd.setBoolean("ambient", pe.isAmbient());
			cpd.setInt("amplifier", pe.getAmplifier());
			cpd.setInt("duration", pe.getDuration());
			cpd.setString("type", pe.getType().getName());
		}

		public static PotionEffect readPotionEffect(NBTTagCompound cpd)
		{
			return new PotionEffect(PotionEffectType.getByName(cpd.getString("type")), cpd.getInt("duration"), cpd.getInt("amplifier"), cpd.getBoolean("ambient"));
		}
	}

	public static class Data
	{
		static CDAPI cdapi = CDAPI.getInstance();
		static Log clog = cdapi.getHandler().getCLog();
		private static Charset utf8 = Charset.forName("UTF-8");

		private Data()
		{
		}

		public static NBTTagCompound load(String path) throws IOException
		{
			return load(path, null);
		}
		public static NBTTagCompound load(String path, Object o) throws IOException
		{
			if (!new File(path).exists())
			{
				clog.log("File " + path + " not found. Returning", o);
				return null;
			}
			FileInputStream inputStream = new FileInputStream(path);
			NBTTagCompound base = NBTCompressedStreamTools.a(inputStream);
			inputStream.close();
			return base;
		}

		public static void save(NBTTagCompound nbtData, String path) throws IOException
		{
			save(nbtData, path, null);
		}
		public static void save(NBTTagCompound nbtData, String path, Object o) throws IOException
		{
			FileOutputStream outputStream = new FileOutputStream(path + "~");
			NBTCompressedStreamTools.a(nbtData, outputStream);
			outputStream.flush();
			outputStream.close();
			clog.log("Finished saving", o);
			clog.log("Moving " + path + "~ to " + path, o);
			File f = new File(path + "~");
			clog.log("Deleting old file", o);
			File old = new File(path);
			if (!old.exists())
				clog.log("File does not exists", o);
			else if (!old.delete())
			{
				clog.log("Error while deleting old file. Throwing exception", o);
				throw new IOException("Cannot delete File " + path);
			}
			else
				clog.log("Old File deleted", o);
			f.renameTo(old);
			clog.log("Moved", o);
		}
		public static void save(String text, String path, Object o) throws IOException
		{
			FileUtils.writeStringToFile(new File(path + "~"), text, utf8, false);
			clog.log("Finished saving", o);
			clog.log("Moving " + path + "~ to " + path, o);
			File f = new File(path + "~");
			clog.log("Deleting old file", o);
			File old = new File(path);
			if (!old.exists())
				clog.log("File does not exists", o);
			else if (!old.delete())
			{
				clog.log("Error while deleting old file. Throwing exception", o);
				throw new IOException("Cannot delete File " + path);
			}
			else
				clog.log("Old File deleted", o);
			f.renameTo(old);
			clog.log("Moved", o);
		}

		public static long getTimestamp()
		{
			return System.currentTimeMillis() / 1000;
		}
		public static String getTime()
		{
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}
		public static Calendar getCalendar()
		{
			return Calendar.getInstance();
		}

		public static String readFile(String path) throws IOException
		{
			return readFile(path, utf8);
		}
		public static String readFile(String path, Charset encoding) throws IOException
		{
			if (!new File(path).exists())
				return null;
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return encoding.decode(ByteBuffer.wrap(encoded)).toString();
		}

		@Deprecated
		public static boolean sendBlockChange(Player p, Location l, Block block, byte tileId)
		{
			return sendBlockChange(p, l, block.fromLegacyData(tileId));
		}
		@Deprecated
		public static boolean sendBlockChange(Player p, Location l, int block, byte tileId)
		{
			if (block > 255)
				return false;
			return sendBlockChange(p, l, Block.getById(block).fromLegacyData(tileId));
		}
		@SuppressWarnings("rawtypes")
		public static boolean sendBlockChange(Player p, Location l, Block block, BlockVariantData... bvd)
		{
			return sendBlockChange(p, l, block.getBlockData(), bvd);
		}
		@SuppressWarnings("rawtypes")
		public static boolean sendBlockChange(Player p, Location l, IBlockData blockData, BlockVariantData... bvd)
		{
			PacketContainer blockPacket = new PacketContainer(PacketType.Play.Server.BLOCK_CHANGE);
			StructureModifier<Object> sm = blockPacket.getModifier();
			for (BlockVariantData currentBVD : bvd)
				blockData = currentBVD.applyTo(blockData);
			sm.write(0, Minecraft.getBlockPosition(l));
			sm.write(1, blockData);
			try
			{
				ProtocolLibrary.getProtocolManager().sendServerPacket(p, blockPacket);
			}
			catch (InvocationTargetException e)
			{
				return false;
			}
			return true;
		}

		public static void messagePlayers(String message, String... permission)
		{
			for (Player p : Bukkit.getOnlinePlayers())
				for (String perm : permission)
					if (permission == null || p.hasPermission(perm))
					{
						p.sendMessage(message);
						break;
					}
		}

		public static void sendPluginMessage(String channel, String subChannel, String message)
		{
			clog.log("Sending PluginMessage in channel '" + channel + "' with subchannel '" + subChannel + "' and message '" + message + "'");
			Bukkit.getServer().sendPluginMessage(CDAPI.getInstance(), channel, createPluginMessageByteArray(subChannel, message));
		}
		public static void sendPluginMessage(String channel, String subChannel, String message, Player p)
		{
			clog.log("Sending PluginMessage in channel '" + channel + "' with subchannel '" + subChannel + "' and message '" + message + "' from Player " + p.getName() + " (" + p.getUniqueId() + ")");
			p.sendPluginMessage(CDAPI.getInstance(), channel, createPluginMessageByteArray(subChannel, message));
		}

		private static byte[] createPluginMessageByteArray(String subChannel, String message)
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos = new DataOutputStream(baos);
			try
			{
				dos.writeUTF(subChannel);
				dos.writeUTF(message);
			}
			catch (Exception x)
			{
				clog.printException(x);
			}
			return baos.toByteArray();
		}

		public static boolean checkPackage(String p)
		{
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			if (stack.length < 4)
				return false;
			return Var.getPackageName(stack[3].getClassName()).equals(p);
		}
		public static boolean checkSamePackage()
		{
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			if (stack.length < 4)
				return false;
			return Var.getPackageName(stack[2].getClassName()).equals(Var.getPackageName(stack[3].getClassName()));
		}

		public static void printException(Throwable t)
		{
			printException(t, System.out);
		}
		public static void printException(Throwable t, PrintStream ps)
		{
			while (t != null)
			{
				ps.print("Exception: " + t.getClass().getName());
				t.printStackTrace(ps);
				t = t.getCause();
			}
		}

		public static void printStackTrace()
		{
			printStackTrace(System.out);
		}
		public static void printStackTrace(PrintStream ps, Thread t)
		{
			for (StackTraceElement ste : t.getStackTrace())
				ps.println(ste.getClassName() + "(" + ste.getFileName() + ":" + ste.getMethodName() + "():" + ste.getLineNumber() + ")");
		}
		public static void printStackTrace(PrintStream ps)
		{
			printStackTrace(ps, Thread.currentThread());
		}
	}

	public static class Minecraft
	{
		private static Lock setTeamLock = new ReentrantLock();
		public static ProtocolManager pm = ProtocolLibrary.getProtocolManager();
		private static Log clog = Log.getInstance();
		private static CraftServer craftServer;
		private static Field onlineUsers;
		private static PacketContainer emptyTitle;
		public static MinecraftServer mcServer;
		
		static
		{
			try
			{
				craftServer = (CraftServer) Bukkit.getServer();
				onlineUsers = craftServer.getClass().getDeclaredField("playerView");
				onlineUsers.setAccessible(true);
			}
			catch (Exception x)
			{
				clog.printException(x);
			}
			emptyTitle = new PacketContainer(PacketType.Play.Server.TITLE);
			emptyTitle.getModifier().write(0, EnumTitleAction.TITLE);
			emptyTitle.getChatComponents().write(0, WrappedChatComponent.fromChatMessage(" ")[0]);
			mcServer = ((CraftServer) Bukkit.getServer()).getServer();
		}

		private Minecraft()
		{
		}


		public enum ParticleType
		{
			HIT_GROUND, DISPENSER_DROP, BLOCK_BREAK
		}

		public enum Attribute
		{
			MOVEMENT_SPEED, ATTACK_DAMAGE, MAX_HEALTH, FOLLOW_RANGE, KNOCKBACK_RESISTANCE
		}

		public enum UpdateMode
		{
			ONLY_PHYSICS(1), ONLY_USER(2), PHYSICS_AND_USER(3), NO_PHYSICS_NO_USER(4);
			public final int id;

			private UpdateMode(int id)
			{
				this.id = id;
			}
		}

		public static org.bukkit.Chunk getChunkAtCoords(int x, int z, boolean load, org.bukkit.World w)
		{
			org.bukkit.Chunk c = w.getChunkAt(x >> 4, z >> 4);
			if (load && !c.isLoaded())
				c.load();
			return c;
		}

		public static org.bukkit.Chunk loadChunkAtCoords(int x, int z, org.bukkit.World w)
		{
			return getChunkAtCoords(x, z, true, w);
		}

		@Deprecated
		public static void setBlockWithUpdate(Location l, Block b, net.minecraft.server.v1_9_R1.World world, int tileId)
		{
			setBlockWithUpdate(l.getBlockX(), l.getBlockY(), l.getBlockZ(), b.fromLegacyData(tileId), world);
		}

		@Deprecated
		public static void setBlockWithUpdate(int x, int y, int z, Block b, net.minecraft.server.v1_9_R1.World world, int tileId)
		{
			setBlockWithUpdate(x, y, z, b.fromLegacyData(tileId), world);
		}
		@Deprecated
		public static void setBlockWithoutUpdate(Location l, Block b, net.minecraft.server.v1_9_R1.World world, int tileId)
		{
			setBlockWithoutUpdate(l.getBlockX(), l.getBlockY(), l.getBlockZ(), b.fromLegacyData(tileId), world);
		}
		@Deprecated
		public static void setBlockWithoutUpdate(int x, int y, int z, Block b, net.minecraft.server.v1_9_R1.World world, int tileId)
		{
			setBlockWithoutUpdate(x, y, z, b.fromLegacyData(tileId), world);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdate(Location l, Block b, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(l.getBlockX(), l.getBlockY(), l.getBlockZ(), b, world, UpdateMode.PHYSICS_AND_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdate(int x, int y, int z, Block b, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(x, y, z, b, world, UpdateMode.PHYSICS_AND_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithoutUpdate(Location l, Block b, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(l.getBlockX(), l.getBlockY(), l.getBlockZ(), b, world, UpdateMode.ONLY_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithoutUpdate(int x, int y, int z, Block b, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(x, y, z, b, world, UpdateMode.ONLY_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdateMode(Location l, Block b, net.minecraft.server.v1_9_R1.World world, UpdateMode mode, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(l.getBlockX(), l.getBlockY(), l.getBlockZ(), b.getBlockData(), world, mode, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdateMode(int x, int y, int z, Block b, net.minecraft.server.v1_9_R1.World world, UpdateMode mode, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(x, y, z, b.getBlockData(), world, mode, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdate(Location l, IBlockData blockData, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(l.getBlockX(), l.getBlockY(), l.getBlockZ(), blockData, world, UpdateMode.PHYSICS_AND_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdate(int x, int y, int z, IBlockData blockData, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(x, y, z, blockData, world, UpdateMode.PHYSICS_AND_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithoutUpdate(Location l, IBlockData blockData, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(l.getBlockX(), l.getBlockY(), l.getBlockZ(), blockData, world, UpdateMode.ONLY_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithoutUpdate(int x, int y, int z, IBlockData blockData, net.minecraft.server.v1_9_R1.World world, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(x, y, z, blockData, world, UpdateMode.ONLY_USER, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdateMode(Location l, IBlockData blockData, net.minecraft.server.v1_9_R1.World world, UpdateMode mode, BlockVariantData... bvd)
		{
			setBlockWithUpdateMode(l.getBlockX(), l.getBlockY(), l.getBlockZ(), blockData, world, mode, bvd);
		}
		@SuppressWarnings("rawtypes")
		public static void setBlockWithUpdateMode(int x, int y, int z, IBlockData blockData, net.minecraft.server.v1_9_R1.World world, UpdateMode mode, BlockVariantData... bvd)
		{
			if (x < -30000000 || z < -30000000 || x >= 30000000 || z >= 30000000 || y < 0 || y >= 256)
				return;
			loadChunkAtCoords(x, z, world.getWorld());
			for (BlockVariantData currentBVD : bvd)
				blockData = currentBVD.applyTo(blockData);
			world.setTypeAndData(getBlockPosition(x, y, z), blockData, mode.id);
		}

		/**
		 * @deprecated Use {@link #kickPlayersWithPerm(String,String)} instead
		 */
		@Deprecated
		public static void kickPerm(String permission, String reason)
		{
			kickPlayersWithPerm(permission, reason);
		}

		public static void kickPlayersWithPerm(String permission, String reason)
		{
			if (permission.length() == 0)
				return;
			boolean b = permission.startsWith("-");
			if (b)
				if (permission.length() >= 2)
					permission = permission.substring(1);
				else
					return;
			for (Player p : getOnlinePlayers())
				if (b ^ p.hasPermission(permission))
					p.kickPlayer(reason);
		}
		public static void kickAll(String reason)
		{
			for (Player p : getOnlinePlayers())
				p.kickPlayer(reason);
		}

		private static int getParticleId(ParticleType pt)
		{
			switch (pt)
			{
				case HIT_GROUND:
					return 2006;
				case DISPENSER_DROP:
					return 2000;
				case BLOCK_BREAK:
					return 2001;
				default:
					return 0;
			}
		}
		
		@Deprecated
		public static PacketContainer sendParticlesToAll(Location l, ParticleType pt, int i) throws InvocationTargetException
		{
			PacketContainer pc = sendParticles(null, l, pt, i, false);
			ProtocolManager pm = ProtocolLibrary.getProtocolManager();
			for (Player p : getOnlinePlayers())
				try
				{
					pm.sendServerPacket(p, pc);
				}
				catch (Exception x)
				{
					x.printStackTrace();
				}
			return pc;
		}
		@Deprecated
		public static PacketContainer sendParticles(Collection<? extends Player> players, Location l, ParticleType pt, int i) throws InvocationTargetException
		{
			PacketContainer pc = sendParticles(null, l, pt, i, false);
			ProtocolManager pm = ProtocolLibrary.getProtocolManager();
			for (Player p : players)
				try
				{
					pm.sendServerPacket(p, pc);
				}
				catch (Exception x)
				{
					x.printStackTrace();
				}
			return pc;
		}
		@Deprecated
		public static PacketContainer sendParticles(Player p, Location l, ParticleType pt, int i, boolean send) throws InvocationTargetException
		{
			PacketContainer pc = new PacketContainer(PacketType.Play.Server.WORLD_EVENT);
			pc.getIntegers().write(0, getParticleId(pt));
			pc.getIntegers().write(1, i);
			pc.getIntegers().write(2, l.getBlockX());
			pc.getIntegers().write(3, l.getBlockY());
			pc.getIntegers().write(4, l.getBlockZ());
			pc.getBooleans().write(0, false);
			if (send) ProtocolLibrary.getProtocolManager().sendServerPacket(p, pc);
			return pc;
		}
		
		public static PacketContainer sendParticlesToAll(Location l, float radius, Particle type, float speed, int count, int extra) throws InvocationTargetException
		{
			l = new Location(l.getWorld(), l.getX() - radius, l.getY() - radius, l.getZ() - radius);
			radius *= 2;
			return sendParticlesToAll(l, radius, radius, radius, type, speed, count, extra);
		}
		public static PacketContainer sendParticlesToAll(Location l, float offX, float offY, float offZ, Particle type, float speed, int count, int extra) throws InvocationTargetException
		{
			return sendParticles(getOnlinePlayers(), l, offX, offY, offZ, type, speed, count, extra);
		}
		public static PacketContainer sendParticles(Collection<? extends Player> players, Location l, float radius, Particle type, float speed, int count, int extra) throws InvocationTargetException
		{
			l = new Location(l.getWorld(), l.getX() - radius, l.getY() - radius, l.getZ() - radius);
			radius *= 2;
			return sendParticles(players, l, radius, radius, radius, type, speed, count, extra);
		}
		public static PacketContainer sendParticles(Collection<? extends Player> players, Location l, float offX, float offY, float offZ, Particle type, float speed, int count, int extra) throws InvocationTargetException
		{
			PacketContainer pc = sendParticles(null, l, offX, offY, offZ, type, speed, count, extra, false);
			ProtocolManager pm = ProtocolLibrary.getProtocolManager();
			for (Player p : players)
				try
				{
					pm.sendServerPacket(p, pc);
				}
				catch (Exception x)
				{
					x.printStackTrace();
				}
			return pc;
		}
		public static PacketContainer sendParticles(Player p, Location l, float radius, Particle type, float speed, int count, int extra, boolean send) throws InvocationTargetException
		{
			l = new Location(l.getWorld(), l.getX() - radius, l.getY() - radius, l.getZ() - radius);
			radius *= 2;
			return sendParticles(p, l, radius, radius, radius, type, speed, count, extra, send);
		}
		public static PacketContainer sendParticles(Player p, Location l, float offX, float offY, float offZ, Particle type, float speed, int count, int extra, boolean send) throws InvocationTargetException
		{
			PacketContainer pc = new PacketContainer(PacketType.Play.Server.WORLD_PARTICLES);
			StructureModifier<Float> floats = pc.getFloat();
			floats.write(0, (float) l.getX());
			floats.write(1, (float) l.getY());
			floats.write(2, (float) l.getZ());
			floats.write(3, offX);
			floats.write(4, offY);
			floats.write(5, offZ);
			floats.write(6, speed);
			pc.getIntegers().write(0, count);
			pc.getModifier().write(0, EnumParticle.valueOf(type.name()));
			pc.getParticles().write(0, type);
			pc.getIntegerArrays().write(0, new int[] { extra });
			if (send)
				ProtocolLibrary.getProtocolManager().sendServerPacket(p, pc);
			return pc;
		}

		public static void send(PacketContainer pc, Player p)
		{
			try
			{
				pm.sendServerPacket(p, pc);
			}
			catch (Exception x)
			{}
		}
		public static void send(PacketContainer pc, Collection<Player> col)
		{
			for (Player p : col)
				try
				{
					pm.sendServerPacket(p, pc);
				}
				catch (Exception x)
				{}
		}

		/**
		 * @deprecated Use {@link #sendPacketToAll(PacketContainer)} instead
		 */
		@Deprecated
		public static void sendAll(PacketContainer pc)
		{
			sendPacketToAll(pc);
		}

		public static void sendPacketToAll(PacketContainer pc)
		{
			ProtocolManager pm = ProtocolLibrary.getProtocolManager();
			for (Player p : Bukkit.getOnlinePlayers())
				try
				{
					pm.sendServerPacket(p, pc);
				}
				catch (Exception x)
				{}
		}

		public static void playSound(Collection<? extends Player> players, Sound s)
		{
			for (Player p : players)
				p.playSound(p.getLocation(), s, 1, 1);
		}
		public static void playSound(Player[] players, Sound s)
		{
			for (Player p : players)
				p.playSound(p.getLocation(), s, 1, 1);
		}
		public static void playSound(Player p, Sound s)
		{
			p.playSound(p.getLocation(), s, 1, 1);
		}

		/**
		 * @deprecated Use {@link #playSoundToAll(Sound)} instead
		 */
		@Deprecated
		public static void playSound(Sound s)
		{
			playSoundToAll(s);
		}
		public static void playSoundToAll(Sound s)
		{
			playSound(Bukkit.getOnlinePlayers(), s);
		}

		public static void setExp(float exp)
		{
			setExp(exp, getOnlinePlayers());
		}
		public static void setExp(float exp, Collection<? extends Player> players)
		{
			for (Player p : players)
				p.setExp(exp);
		}
		public static void setExp(float exp, Player p)
		{
			p.setExp(exp);
		}

		public static void setLevel(int level)
		{
			setLevel(level, getOnlinePlayers());
		}
		public static void setLevel(int level, Collection<? extends Player> players)
		{
			for (Player p : players)
				p.setLevel(level);
		}
		public static void setLevel(int level, Player p)
		{
			p.setLevel(level);
		}

		public static void setLevelAndExp(int level, float exp)
		{
			setLevelAndExp(level, exp, getOnlinePlayers());
		}
		public static void setLevelAndExp(int level, float exp, Collection<? extends Player> players)
		{
			for (Player p : players)
			{
				p.setLevel(level);
				p.setExp(exp);
			}
		}
		public static void setLevelAndExp(int level, float exp, Player p)
		{
			p.setLevel(level);
			p.setExp(exp);
		}

		public static void setAttribute(LivingEntity e, double amount, Attribute a)
		{
			EntityLiving nmsEntity = ((CraftLivingEntity) e).getHandle();
			AttributeMapBase amb = nmsEntity.getAttributeMap();
			AttributeInstance ai;
			try
			{
				ai = amb.b(getMCAttribute(a));
			}
			catch (IllegalArgumentException x)
			{
				ai = amb.a(getMCAttribute(a));
			}
			ai.setValue(amount);
		}

		public static IAttribute getMCAttribute(Attribute a)
		{
			switch (a)
			{
				case MAX_HEALTH:
					return GenericAttributes.maxHealth;
				case FOLLOW_RANGE:
					return GenericAttributes.FOLLOW_RANGE;
				case KNOCKBACK_RESISTANCE:
					return GenericAttributes.c;
				case MOVEMENT_SPEED:
					return GenericAttributes.MOVEMENT_SPEED;
				case ATTACK_DAMAGE:
					return GenericAttributes.ATTACK_DAMAGE;
			}
			return null;
		}

		public static void setTeam(String entryName, Team t)
		{
			setTeam(entryName, t, false, null);
		}
		public static void setTeam(String entryName, Team t, boolean setPlayerListName)
		{
			setTeam(entryName, t, setPlayerListName, entryName);
		}
		public static void setTeam(String entryName, String team, Scoreboard s)
		{
			setTeam(entryName, team, s, false);
		}
		public static void setTeam(String entryName, String team, Scoreboard s, boolean setPlayerListName)
		{
			setTeam(entryName, team, s, setPlayerListName, entryName);
		}
		public static void setTeam(String entryName, String team, Scoreboard s, boolean setPlayerListName, String name)
		{
			Team t = s.getTeam(team);
			if (t == null)
				return;
			setTeam(entryName, t, setPlayerListName, name);
		}
		public static void setTeam(String entryName, Team t, boolean setPlayerListName, String listName)
		{
			try
			{
				setTeamLock.lock();
				Player onlinePlayer;
				if (setPlayerListName && (onlinePlayer = Bukkit.getPlayer(entryName)) != null)
					onlinePlayer.setPlayerListName(Var.cut(t.getPrefix() + listName, 16));
				Team prev = t.getScoreboard().getEntryTeam(entryName);
				if (prev != null)
				{
					if (prev.getName() == t.getName())
						return;
					prev.removeEntry(entryName);
				}
				t.addEntry(entryName);
			}
			finally
			{
				setTeamLock.unlock();
			}
		}

		public static void sendMessage(Collection<? extends Player> players, String message)
		{
			for (Player p : players)
				p.sendMessage(message);
		}
		public static void sendMessage(Player[] players, String message)
		{
			for (Player p : players)
				p.sendMessage(message);
		}

		public static PacketContainer[] sendTitle(String text, EnumTitleAction a, int[] times)
		{
			return sendTitle(WrappedChatComponent.fromText(text), a, getOnlinePlayers(), times, false);
		}
		public static PacketContainer[] sendTitle(String text, EnumTitleAction a, int[] times, boolean force)
		{
			return sendTitle(WrappedChatComponent.fromText(text), a, getOnlinePlayers(), times, force);
		}
		public static PacketContainer[] sendTitle(String text, EnumTitleAction a, Collection<? extends Player> players, int[] times, boolean force)
		{
			return sendTitle(WrappedChatComponent.fromText(text), a, players, times, force);
		}
		public static PacketContainer[] sendTitle(String text, EnumTitleAction a, Collection<? extends Player> players, int[] times)
		{
			return sendTitle(WrappedChatComponent.fromText(text), a, players, times, false);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wcc, EnumTitleAction a, int[] times)
		{
			return sendTitle(wcc, a, getOnlinePlayers(), times, false);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wcc, EnumTitleAction a, int[] times, boolean force)
		{
			return sendTitle(wcc, a, getOnlinePlayers(), times, force);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wcc, EnumTitleAction a, Collection<? extends Player> players, int[] times)
		{
			return sendTitle(wcc, a, players, times, true);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wcc, EnumTitleAction a, Collection<? extends Player> players, int[] times, boolean sendTimes)
		{
			PacketContainer pc = new PacketContainer(PacketType.Play.Server.TITLE), pc2 = null;
			if (sendTimes)
			{
				pc2 = new PacketContainer(PacketType.Play.Server.TITLE);
				pc2.getModifier().write(0, EnumTitleAction.TIMES);
				pc2.getIntegers().write(0, times[0]);
				pc2.getIntegers().write(1, times[1]);
				pc2.getIntegers().write(2, times[2]);
			}
			pc.getModifier().write(0, a);
			pc.getChatComponents().write(0, wcc);
			if (players != null)
				for (Player p : players)
				try
				{
				if (pc2 != null)
					pm.sendServerPacket(p, pc2);
				pm.sendServerPacket(p, pc);
				if (a == EnumTitleAction.SUBTITLE)
					pm.sendServerPacket(p, emptyTitle);
				}
				catch (InvocationTargetException x)
				{
				if (clog.isGlobalDebugEnabled())
					clog.printException(x);
				}
			if (pc2 != null)
				return new PacketContainer[] { pc, pc2 };
			return new PacketContainer[] { pc };
		}
		public static PacketContainer[] sendTitle(String title, String subTitle, int[] times)
		{
			return sendTitle(WrappedChatComponent.fromText(title), WrappedChatComponent.fromText(subTitle), getOnlinePlayers(), times, false);
		}
		public static PacketContainer[] sendTitle(String title, String subTitle, int[] times, boolean force)
		{
			return sendTitle(WrappedChatComponent.fromText(title), WrappedChatComponent.fromText(subTitle), getOnlinePlayers(), times, force);
		}
		public static PacketContainer[] sendTitle(String title, String subTitle, Collection<? extends Player> players, int[] times, boolean force)
		{
			return sendTitle(WrappedChatComponent.fromText(title), WrappedChatComponent.fromText(subTitle), players, times, force);
		}
		public static PacketContainer[] sendTitle(String title, String subTitle, Collection<? extends Player> players, int[] times)
		{
			return sendTitle(WrappedChatComponent.fromText(title), WrappedChatComponent.fromText(subTitle), players, times, false);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wccTitle, WrappedChatComponent wccSubTitle, int[] times)
		{
			return sendTitle(wccTitle, wccSubTitle, getOnlinePlayers(), times, false);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wccTitle, WrappedChatComponent wccSubTitle, int[] times, boolean force)
		{
			return sendTitle(wccTitle, wccSubTitle, getOnlinePlayers(), times, force);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wccTitle, WrappedChatComponent wccSubTitle, Collection<? extends Player> players, int[] times)
		{
			return sendTitle(wccTitle, wccSubTitle, players, times, true);
		}
		public static PacketContainer[] sendTitle(WrappedChatComponent wccTitle, WrappedChatComponent wccSubTitle, Collection<? extends Player> players, int[] times, boolean sendTimes)
		{
			PacketContainer pcTitle = new PacketContainer(PacketType.Play.Server.TITLE), pcSubtitle = new PacketContainer(PacketType.Play.Server.TITLE), pc2 = null;
			if (sendTimes)
			{
				pc2 = new PacketContainer(PacketType.Play.Server.TITLE);
				pc2.getModifier().write(0, EnumTitleAction.TIMES);
				pc2.getIntegers().write(0, times[0]);
				pc2.getIntegers().write(1, times[1]);
				pc2.getIntegers().write(2, times[2]);
			}
			pcTitle.getModifier().write(0, EnumTitleAction.TITLE);
			pcTitle.getChatComponents().write(0, wccTitle);
			pcSubtitle.getModifier().write(0, EnumTitleAction.SUBTITLE);
			pcSubtitle.getChatComponents().write(0, wccSubTitle);
			if (players != null)
				for (Player p : players)
				try
				{
				if (pc2 != null)
					pm.sendServerPacket(p, pc2);
				pm.sendServerPacket(p, pcTitle);
				pm.sendServerPacket(p, pcSubtitle);
				}
				catch (InvocationTargetException x)
				{
				if (clog.isGlobalDebugEnabled())
					clog.printException(x);
				}
			if (pc2 != null)
				return new PacketContainer[] { pcTitle, pcSubtitle, pc2 };
			return new PacketContainer[] { pcTitle, pcSubtitle };
		}

		public static void sendTitle(PacketContainer pc)
		{
			sendTitle(pc, null, null, getOnlinePlayers());
		}
		public static void sendTitle(PacketContainer pc, Collection<? extends Player> players)
		{
			sendTitle(pc, null, null, players);
		}
		public static void sendTitle(PacketContainer pc, PacketContainer pcTimes)
		{
			sendTitle(pc, pcTimes, null, getOnlinePlayers());
		}
		public static void sendTitle(PacketContainer pc, PacketContainer pcTimes, Collection<? extends Player> players)
		{
			sendTitle(pc, pcTimes, null, players);
		}
		public static void sendTitle(PacketContainer pc, EnumTitleAction a)
		{
			sendTitle(pc, null, a, getOnlinePlayers());
		}
		public static void sendTitle(PacketContainer pc, EnumTitleAction a, Collection<? extends Player> players)
		{
			sendTitle(pc, null, a, players);
		}
		public static void sendTitle(PacketContainer pc, PacketContainer pcTimes, EnumTitleAction a)
		{
			sendTitle(pc, pcTimes, a, getOnlinePlayers());
		}
		public static void sendTitle(PacketContainer pc, PacketContainer pcTimes, EnumTitleAction a, Collection<? extends Player> players)
		{
			if (a == null)
				a = (EnumTitleAction) pc.getModifier().read(0);
			for (Player p : players)
				try
				{
					if (pcTimes != null)
						pm.sendServerPacket(p, pcTimes);
					pm.sendServerPacket(p, pc);
					if (a == EnumTitleAction.SUBTITLE)
						pm.sendServerPacket(p, emptyTitle);
				}
				catch (InvocationTargetException x)
				{
					if (clog.isGlobalDebugEnabled())
						clog.printException(x);
				}
		}

		public static String getHeartString(Damageable d)
		{
			return getHeartString(d.getHealth(), d.getMaxHealth());
		}
		public static String getHeartString(double h, double max)
		{
			int[] heartNumbers = getHeartNumbers10(h, max);
			return ChatColor.RED + Var.expandString("\u2764", heartNumbers[0]) + ChatColor.WHITE + Var.expandString("\u2764", heartNumbers[1] - heartNumbers[0]);
		}
		public static int[] getHeartNumbers10(Damageable d)
		{
			return getHeartNumbers10(d.getHealth(), d.getMaxHealth());
		}
		public static int[] getHeartNumbers10(double h, double max)
		{
			int ih, imax;
			if (h % 1 == 0)
				ih = (int) h;
			else
				ih = (int) (h + 1);
			if (max % 1 == 0)
				imax = (int) max;
			else
				imax = (int) (max + 1);
			ih = (ih + 1) / 2;
			imax = (imax + 1) / 2;
			return new int[] { ih, imax };
		}
		public static int[] getHeartNumbers20(Damageable d)
		{
			return getHeartNumbers20(d.getHealth(), d.getMaxHealth());
		}
		public static int[] getHeartNumbers20(double h, double max)
		{
			int ih, imax;
			if (h % 1 == 0)
				ih = (int) h;
			else
				ih = (int) (h + 1);
			if (max % 1 == 0)
				imax = (int) max;
			else
				imax = (int) (max + 1);
			return new int[] { ih, imax };
		}

		@SuppressWarnings("unchecked")
		public static List<Player> getOnlinePlayers()
		{
			try
			{
				return (List<Player>) onlineUsers.get(craftServer);
			}
			catch (IllegalArgumentException | IllegalAccessException x)
			{
				clog.printException(x);
				return null;
			}
		}

		public static boolean isValid(ItemStack i)
		{
			return i != null && i.getType() != Material.AIR && i.getAmount() >= 1;
		}

		public static Collection<Player> toOnlinePlayers(Collection<UUID> col)
		{
			Set<Player> set = new HashSet<Player>();
			Player p;
			for (UUID u : col)
				if ((p = Bukkit.getPlayer(u)) != null)
					set.add(p);
			return set;
		}

		public static Set<org.bukkit.block.Block> getAdjacentBlocks(org.bukkit.block.Block b, int radius)
		{
			Set<org.bukkit.block.Block> back = new HashSet<org.bukkit.block.Block>();
			if (radius < 1)
				return back;
			for (int i = 0; i < radius; ++i)
				b = b.getRelative(BlockFace.SOUTH).getRelative(BlockFace.EAST);
			radius *= 2;
			for (BlockFace bf : new BlockFace[] { BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH })
				for (int i = 0; i < radius; ++i)
					back.add(b = b.getRelative(bf));
			return back;
		}
		public static Set<org.bukkit.block.Block> getAdjacentBlocks(org.bukkit.block.Block b, int radiusFrom, int radiusTo)
		{
			Set<org.bukkit.block.Block> back = new HashSet<org.bukkit.block.Block>();
			for (; radiusFrom <= radiusTo; ++radiusFrom)
				back.addAll(getAdjacentBlocks(b, radiusFrom));
			return back;
		}

		public static BlockPosition getBlockPosition(Location l)
		{
			return new BlockPosition(l.getBlockX(), l.getBlockY(), l.getBlockZ());
		}
		public static BlockPosition getBlockPosition(int x, int y, int z)
		{
			return new BlockPosition(x, y, z);
		}

		public static Location getLocation(BlockPosition bp, World w)
		{
			return new Location(w, bp.getX(), bp.getY(), bp.getZ());
		}

		public static net.minecraft.server.v1_9_R1.World getMCWorld(World w)
		{
			return ((CraftWorld) w).getHandle();
		}


		public static enum Food
		{
			APPLE(4, 0.6F), BAKED_POTATO(5, 1.44F), BREAD(5, 1.2F), CAKE_PIECE(2, 0.2F), CAKE(14, 1.4F), CARROT(3, 1.6F), CLOWNFISH(1, 0.2F), COOKED_CHICKEN(6, 1.2F), COOKED_FISH(5, 1.2F), COOKED_MUTTON(6, 1.6F), COOKED_PORKCHOP(8, 1.6F), COOKED_RABBIT(5, 1.2F), COOKED_SALMON(6, 1.6F), COOKIE(2, 0.2F), GOLDEN_APPLE(4, 2.4F), ENCHANTED_GOLDEN_APPLE(4, 2.4F), GOLDEN_CARROT(6, 2.4F), MELON(6, 2.4F), MUSHROOM_SOUP(6, 1.2F), POISONOUS_POTATO(2, 0.6F), POTATO(1, 0.6F), PUFFERFISH(1, 0.2F), PUMPKIN_PIE(8, 0.6F), RABBIT_STEW(10, 1.2F), RAW_BEEF(3, 0.6F), RAW_CHICKEN(2, 0.6F), RAW_FISH(2, 0.2F), RAW_MUTTON(2, 0.6F), RAW_PORKCHOP(3, 0.6F), RAW_RABBIT(2, 0.9F), RAW_SALMON(2, 0.2F), ROTTEN_FLESH(4, 0.2F), SPIDER_EYE(2, 1.6F), STEAK(8, 1.6F);
			int food;
			float saturation;

			Food(int food, float saturation)
			{
				this.food = food;
				this.saturation = saturation;
			}

			public boolean applyToPlayer(Player p)
			{
				if (p.getFoodLevel() >= 20)
					return false;
				int foodLevel = Math.max(p.getFoodLevel() + this.food, 20);
				p.setFoodLevel(foodLevel);
				float saturationLevel = Math.max(p.getSaturation() + this.saturation, foodLevel);
				p.setSaturation(saturationLevel);
				return true;
			}

			@SuppressWarnings("deprecation")
			public Food valueOf(ItemStack i)
			{
				if (i == null)
					return null;
				Food f = valueOf(i.getType());
				if (i.getType() == Material.RAW_FISH)
					switch (i.getData().getData())
					{
					case 1:
					f = Food.RAW_SALMON;
					break;
					case 2:
					f = Food.CLOWNFISH;
					break;
					case 3:
					f = Food.PUFFERFISH;
					break;
					}
				return f;
			}

			public Food valueOf(Material m)
			{
				switch (m)
				{
					case APPLE:
						return Food.APPLE;
					case BAKED_POTATO:
						return Food.BAKED_POTATO;
					case BREAD:
						return Food.BREAD;
					case CAKE:
						return Food.CAKE;
					case CARROT:
						return Food.CARROT;
					case COOKED_CHICKEN:
						return Food.COOKED_CHICKEN;
					case COOKED_FISH:
						return Food.COOKED_FISH;
					// case COOKED_MUTTON:
					// return Food.COOKED_MUTTON;
					// case COOKED_PORKCHOP:
					// return Food.COOKED_PORKCHOP;
					// case COOKED_RABBIT:
					// return Food.COOKED_RABBIT;
					// case COOKED_SALMON:
					// return Food.COOKED_SALMON;
					case COOKIE:
						return Food.COOKIE;
					case GOLDEN_APPLE:
						return Food.GOLDEN_APPLE;
					case GOLDEN_CARROT:
						return Food.GOLDEN_CARROT;
					case MELON:
						return Food.MELON;
					case MUSHROOM_SOUP:
						return Food.MUSHROOM_SOUP;
					case POISONOUS_POTATO:
						return Food.POISONOUS_POTATO;
					case POTATO:
						return Food.POTATO;
					case PUMPKIN_PIE:
						return Food.PUMPKIN_PIE;
					// case RABBIT_STEW:
					// return Food.RABBIT_STEW;
					case RAW_BEEF:
						return Food.RAW_BEEF;
					case RAW_CHICKEN:
						return Food.RAW_CHICKEN;
					case RAW_FISH:
						return Food.RAW_FISH;
					// case RAW_MUTTON:
					// return Food.RAW_MUTTON;
					case PORK:
						return Food.RAW_PORKCHOP;
					// case RAW_RABBIT:
					// return Food.RAW_RABBIT;
					case ROTTEN_FLESH:
						return Food.ROTTEN_FLESH;
					case SPIDER_EYE:
						return Food.SPIDER_EYE;
					case COOKED_BEEF:
						return Food.STEAK;
					default:
						return null;
				}
			}
		}


		public static class JsonParser
		{
			private JsonParser()
			{
			}

			public static String parse(String json)
			{
				try
				{
					if (json == null || json.equals(""))
						return null;
					if (json.equals("\"\""))
						return "\"\"";
					return getFullString(getExtraArray(json));
				}
				catch (Exception x)
				{
					return json;
				}
			}

			private static JsonArray getExtraArray(String json)
			{
				JsonElement jelement = new com.google.gson.JsonParser().parse(json);
				return jelement.getAsJsonObject().get("extra").getAsJsonArray();
			}

			private static String getFullString(JsonArray jarray)
			{
				String back = "";
				JsonElement jelement;
				for (int i = 0; i < jarray.size(); i++)
				{
					jelement = jarray.get(i);
					if (jelement.isJsonPrimitive())
						back += jelement.getAsString();
					else
						back += getChatString(jelement.getAsJsonObject());
				}
				return back;
			}

			private static String getChatString(JsonObject jobj)
			{
				String back = "";
				String colors = "";
				String mods = "";
				for (Entry<String, JsonElement> jentry : jobj.entrySet())
				{
					switch (jentry.getKey().toLowerCase())
					{
						case "text":
							back = jentry.getValue().getAsString();
							break;
						case "color":
							colors = ChatColor.valueOf(jentry.getValue().getAsString().toUpperCase()) + colors;
							break;
						case "italic":
						case "bold":
						case "underline":
						case "strikethrough":
							if (jentry.getValue().getAsBoolean())
								mods = ChatColor.valueOf(jentry.getKey().toUpperCase()) + mods;
							break;
					}
				}
				return colors + mods + back;
			}
		}
	}
}

package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.BlockBed.EnumBedPart;
import net.minecraft.server.v1_9_R1.BlockCobbleWall.EnumCobbleVariant;
import net.minecraft.server.v1_9_R1.BlockDirt.EnumDirtVariant;
import net.minecraft.server.v1_9_R1.BlockDoor.EnumDoorHalf;
import net.minecraft.server.v1_9_R1.BlockDoor.EnumDoorHinge;
import net.minecraft.server.v1_9_R1.BlockDoubleStepAbstract.EnumStoneSlabVariant;
import net.minecraft.server.v1_9_R1.BlockFlowerPot.EnumFlowerPotContents;
import net.minecraft.server.v1_9_R1.BlockFlowers.EnumFlowerVarient;
import net.minecraft.server.v1_9_R1.BlockHugeMushroom.EnumHugeMushroomVariant;
import net.minecraft.server.v1_9_R1.BlockLogAbstract.EnumLogRotation;
import net.minecraft.server.v1_9_R1.BlockLongGrass.EnumTallGrassType;
import net.minecraft.server.v1_9_R1.BlockMinecartTrackAbstract.EnumTrackPosition;
import net.minecraft.server.v1_9_R1.BlockMonsterEggs.EnumMonsterEggVarient;
import net.minecraft.server.v1_9_R1.BlockPistonExtension.EnumPistonType;
import net.minecraft.server.v1_9_R1.BlockPrismarine.EnumPrismarineVariant;
import net.minecraft.server.v1_9_R1.BlockQuartz.EnumQuartzVariant;
import net.minecraft.server.v1_9_R1.BlockRedSandstone.EnumRedSandstoneVariant;
import net.minecraft.server.v1_9_R1.BlockRedstoneComparator.EnumComparatorMode;
import net.minecraft.server.v1_9_R1.BlockSand.EnumSandVariant;
import net.minecraft.server.v1_9_R1.BlockSandStone.EnumSandstoneVariant;
import net.minecraft.server.v1_9_R1.BlockSmoothBrick.EnumStonebrickType;
import net.minecraft.server.v1_9_R1.BlockStairs.EnumHalf;
import net.minecraft.server.v1_9_R1.BlockStairs.EnumStairShape;
import net.minecraft.server.v1_9_R1.BlockStepAbstract.EnumSlabHalf;
import net.minecraft.server.v1_9_R1.BlockStone.EnumStoneVariant;
import net.minecraft.server.v1_9_R1.BlockTallPlant.EnumTallFlowerVariants;
import net.minecraft.server.v1_9_R1.BlockTallPlant.EnumTallPlantHalf;
import net.minecraft.server.v1_9_R1.BlockTrapdoor.EnumTrapdoorHalf;
import net.minecraft.server.v1_9_R1.BlockWood;
import net.minecraft.server.v1_9_R1.BlockWood.EnumLogVariant;
import net.minecraft.server.v1_9_R1.EnumColor;
import net.minecraft.server.v1_9_R1.EnumDirection;
import net.minecraft.server.v1_9_R1.EnumDirection.EnumAxis;

public class BlockVariant
{
	
	public static class ANVIL
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Damage
		{
			public static IntData DAMAGE(int damage_0_2)
			{
				return new IntData("damage", damage_0_2, 2);
			}
		}
	}
	
	public static class BANNER
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Rotation extends _ROTATION
		{ }
	}
	
	public static class BED
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class BedPart
		{
			public static EnumData<EnumBedPart> HEAD = new EnumData<EnumBedPart>("part", EnumBedPart.HEAD);
			public static EnumData<EnumBedPart> FOOT = new EnumData<EnumBedPart>("part", EnumBedPart.FOOT);
		}
		
		public static class Occupied
		{
			public static BoolData NOT_OCCUPIED = new BoolData("occupied", false);
			public static BoolData OCCUPIED = new BoolData("occupied", true);
		}
	}
	
	public static class BREWING_STAND
	{
		public static class HasBottleEast
		{
			public static BoolData NO_BOTTLE = new BoolData("has_bottle_0", false);
			public static BoolData BOTTLE = new BoolData("has_bottle_0", true);
		}
		public static class HasBottleNorthEast
		{
			public static BoolData NO_BOTTLE = new BoolData("has_bottle_1", false);
			public static BoolData BOTTLE = new BoolData("has_bottle_1", true);
		}
		public static class HasBottleSouthEast
		{
			public static BoolData NO_BOTTLE = new BoolData("has_bottle_2", false);
			public static BoolData BOTTLE = new BoolData("has_bottle_2", true);
		}
	}
	
	public static class BUTTON
	{
		public static class Facing extends _FACING
		{ }
		
		public static class Powered extends _POWERED
		{ }
	}
	
	public static class CACTUS
	{
		public static class Age
		{
			public static IntData AGE(int age_0_15)
			{
				return new IntData("age", age_0_15, 15);
			}
		}
	}
	
	public static class CAKE
	{
		public static class Bites
		{
			public static IntData BITES(int bites_0_6)
			{
				return new IntData("bites", bites_0_6, 6);
			}
		}
	}
	
	public static class CARPET
	{
		public static class Color extends _COLOR
		{ }
	}
	
	public static class CARROTS
	{
		public static class Age extends BlockCrops.Age
		{ }
	}
	
	public static class CAULDRON
	{
		public static class FillLevel
		{
			public IntData FILL_LEVEL(int level_0_3)
			{
				return new IntData("level", level_0_3, 3);
			}
		}
	}
	
	public static class CHEST
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
	}
	
	public static class COBBLE_WALL
	{
		public static class ConnectedUp extends _CONNECTED.ConnectedUp
		{ }
		
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class CobbleVariant
		{
			public static EnumData<EnumCobbleVariant> VARIANT(EnumCobbleVariant variant)
			{
				return new EnumData<EnumCobbleVariant>("variant", variant);
			}
		}
	}
	
	public static class COCOA
	{
		public static class FACING extends _FACING_HORIZONTAL
		{ }
		
		public static class Age
		{
			public static IntData AGE(int age_0_2)
			{
				return new IntData("age", age_0_2, 2);
			}
		}
	}
	
	public static class COMMAND_BLOCK
	{
		public static class Triggered
		{
			public static BoolData NOT_TRIGGERED = new BoolData("triggered", false);
			public static BoolData TRIGGERED = new BoolData("triggered", true);
		}
	}
	
	static class BlockCrops
	{
		public static class Age extends _PLANTS_AGE
		{ }
	}
	
	public static class DAYLIGHT_DETECTOR
	{
		public static class Power
		{
			public static IntData POWER(int power_0_15)
			{
				return new IntData("power", power_0_15, 15);
			}
		}
	}
	
	public static class DIRT
	{
		public static class Variant
		{
			public static EnumData<EnumDirtVariant> VARIANT(EnumDirtVariant variant)
			{
				return new EnumData<EnumDirtVariant>("variant", variant);
			}
		}
		
		public static class Snowy
		{
			public static BoolData NOT_SNOWY = new BoolData("snowy", false);
			public static BoolData SNOWY = new BoolData("snowy", true);
		}
	}
	
	public static class DISPENSER
	{
		public static class Facing extends _FACING
		{ }
		
		public static class Triggered
		{
			public static BoolData NOT_TRIGGERED = new BoolData("triggered", false);
			public static BoolData TRIGGERED = new BoolData("triggered", true);
		}
	}
	
	public static class DOOR
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Opened extends _OPENED
		{ }
		
		public static class Hinge
		{
			public static EnumData<EnumDoorHinge> HINGE(EnumDoorHinge hinge)
			{
				return new EnumData<EnumDoorHinge>("hinge", hinge);
			}
		}
		
		public static class Powered extends _POWERED
		{ }
		
		public static class DoorHalf
		{
			public static EnumData<EnumDoorHalf> DOOR_HALF(EnumDoorHalf doorHalf)
			{
				return new EnumData<EnumDoorHalf>("half", doorHalf);
			}
		}
	}
	
	public static class DOUBLE_STEP
	{
		public static class Seamless
		{
			public static BoolData NOT_SEAMLESS = new BoolData("seamless", false);
			public static BoolData SEAMLESS = new BoolData("seamless", true);
		}
		
		public static class Variant
		{
			public static EnumData<EnumStoneSlabVariant> VARIANT(EnumStoneSlabVariant variant)
			{
				return new EnumData<EnumStoneSlabVariant>("variant", variant);
			}
		}
		
		public static class Half
		{
			public static EnumData<EnumSlabHalf> HALF(EnumSlabHalf half)
			{
				return new EnumData<EnumSlabHalf>("half", half);
			}
		}
	}
	
	public static class DROPPER
	{
		public static class Facing extends DISPENSER.Facing
		{ }
		
		public static class Triggered extends DISPENSER.Triggered
		{ }
	}
	
	public static class ENDER_CHEST
	{
		public static class Facing extends CHEST.Facing
		{ }
	}
	
	public static class ENDER_PORTAL_FRAME
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class HasEye
		{
			public static BoolData NO_EYE = new BoolData("eye", false);
			public static BoolData EYE = new BoolData("eye", true);
		}
	}
	
	public static class FENCE
	{
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedNorth
		{ }
	}
	
	public static class FENCE_GATE
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Opened extends _OPENED
		{ }
		
		public static class Powered extends _POWERED
		{ }
		
		public static class InWall
		{
			public static BoolData NOT_IN_WALL = new BoolData("in_wall", false);
			public static BoolData IN_WALL = new BoolData("in_wall", true);
		}
	}
	
	public static class FIRE
	{
		public static class Age
		{
			public static IntData AGE(int age_0_15)
			{
				return new IntData("age", age_0_15, 15);
			}
		}
		
		public static class Flipped
		{
			public static BoolData NOT_FLIPPED = new BoolData("flipp", false);
			public static BoolData FLIPPED = new BoolData("flipp", true);
		}
		public static class Alt
		{
			public static BoolData NOT_ALT = new BoolData("alt", false);
			public static BoolData ALT = new BoolData("alt", true);
		}
		
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedNorth
		{ }
	
		public static class Upper
		{
			public static BoolData NOT_UPPER = new BoolData("upper", false);
			public static BoolData UPPER = new BoolData("upper", true);
		}
	}
	
	public static class FLOOR_SIGN
	{
		public static class Rotation extends _ROTATION
		{ }
	}
	
	public static class FLOWER_POT
	{
		public static class LegacyData
		{
			public static IntData LEGACY_DATA(int legacyData_0_15)
			{
				return new IntData("legacy_data", legacyData_0_15, 15);
			}
		}
		
		public static class Content
		{
			public static EnumData<EnumFlowerPotContents> CONTENT(EnumFlowerPotContents content)
			{
				return new EnumData<EnumFlowerPotContents>("contents", content);
			}
		}
	}
	
	public static class BlockFlowers
	{
		public static class Type
		{
			public static EnumData<EnumFlowerVarient> TYPE(EnumFlowerVarient type)
			{
				return new EnumData<EnumFlowerVarient>("type", type);
			}
		}
	}
	
	static class BlockFluid
	{
		public static class Level
		{
			public static IntData LEVEL(int level_0_15)
			{
				return new IntData("level", level_0_15, 15);
			}
		}
	}

	public static class FURNACE
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
	}
	
	public static class GRASS
	{
		public static class Snowy extends DIRT.Snowy
		{ }
	}
	
	public static class HAY
	{
		public static class Axis extends _AXIS
		{ }
	}
	
	public static class HOPPER
	{
		public static class Facing
		{
			public static final DirectionData NORTH = _FACING.NORTH;
			public static final DirectionData EAST = _FACING.EAST;
			public static final DirectionData SOUTH = _FACING.SOUTH;
			public static final DirectionData WEST = _FACING.WEST;
			public static final DirectionData DOWN = _FACING.DOWN;
		}
	}
	
	public static class HUGE_MUSHROOM
	{
		public static class Variant
		{
			public static EnumData<EnumHugeMushroomVariant> VARIANT(EnumHugeMushroomVariant variant)
			{
				return new EnumData<EnumHugeMushroomVariant>("variant", variant);
			}
		}
	}
	
	public static class JUKEBOX
	{
		public static class HasRecord
		{
			public static BoolData NO_RECORD = new BoolData("has_record", false);
			public static BoolData HAS_RECORD = new BoolData("has_record", true);
		}
	}
	
	public static class LADDER
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
	}
	
	public static class LAVA
	{
		public static class Level extends BlockFluid.Level
		{ }
	}
	
	public static class LEAVES
	{
		public static class Decayable
		{
			public static BoolData NOT_DECAYABLE = new BoolData("decayable", false);
			public static BoolData DECAYABLE = new BoolData("decayable", true);
		}
		
		public static class CheckDecay
		{
			public static BoolData NO_CHECK = new BoolData("check_decay", false);
			public static BoolData CHECK = new BoolData("check_decay", true);
		}
		
		public static class Variant
		{
			public static EnumData<BlockWood.EnumLogVariant> VARIANT(BlockWood.EnumLogVariant variant)
			{
				return new EnumData<BlockWood.EnumLogVariant>("variant", variant);
			}
		}
	}
	
	public static class LEVER
	{
		public static class Facing extends _FACING
		{ }
		
		public static class Powered
		{
			public static BoolData NOT_POWERED = new BoolData("powered", false);
			public static BoolData POWERED = new BoolData("powered", true);
		}
	}
	
	public static class LOG
	{
		public static class Axis
		{
			public static EnumData<EnumLogRotation> AXIS(EnumLogRotation axis)
			{
				return new EnumData<EnumLogRotation>("axis", axis);
			}
		}
		
		public static class Variant
		{
			public static EnumData<EnumLogVariant> VARIANT(EnumLogVariant variant)
			{
				return new EnumData<EnumLogVariant>("variant", variant);
			}
		}
	}
	
	public static class LONG_GRASS
	{
		public static class Type
		{
			public static EnumData<EnumTallGrassType> TYPE(EnumTallGrassType type)
			{
				return new EnumData<EnumTallGrassType>("type", type);
			}
		}
	}
	
	public static class MINECART_DETECTOR
	{
		public static class Shape
		{
			public static EnumData<EnumTrackPosition> SHAPE(EnumTrackPosition trackPosition)
			{
				return new EnumData<EnumTrackPosition>("shape", trackPosition);
			}
		}
	}
	
	public static class MINECART_TRACK
	{
		public static class Shape extends MINECART_DETECTOR.Shape
		{ }
	}
	
	public static class MONSTER_EGG
	{
		public static class Variant
		{
			public static EnumData<EnumMonsterEggVarient> VARIANT(EnumMonsterEggVarient variant)
			{
				return new EnumData<EnumMonsterEggVarient>("variant", variant);
			}
		}
	}
	
	public static class MYCEL
	{
		public static class Snowy extends DIRT.Snowy
		{ }
	}
	
	public static class NETHER_WART
	{
		public static class Age
		{
			public static IntData AGE(int age_0_3)
			{
				return new IntData("age", age_0_3, 3);
			}
		}
	}
	
	public static class PISTON
	{
		public static class Facing extends _FACING
		{ }
		
		public static class Extended
		{
			public static BoolData NOT_EXTENDED = new BoolData("extended", false);
			public static BoolData EXTENDED = new BoolData("extended", true);
		}
	}
	
	public static class PISTON_EXTENSION
	{
		public static class Facing extends _FACING
		{ }
		
		public static class Type
		{
			public static EnumData<EnumPistonType> TYPE(EnumPistonType type)
			{
				return new EnumData<EnumPistonType>("type", type);
			}
		}
		
		public static class Short
		{
			public static BoolData NOT_SHORT = new BoolData("short", false);
			public static BoolData SHORT = new BoolData("short", true);
		}
	}
	
	public static class PISTON_MOVING //Block 36
	{
		public static class Facing extends _FACING
		{ }
		
		public static class Type extends PISTON_EXTENSION.Type
		{ }
	}
	
	public static class PORTAL
	{
		public static class Axis extends _AXIS
		{ }
	}
	
	public static class POTATOES
	{
		public static class Age extends BlockCrops.Age
		{ }
	}
	
	public static class POWERED_RAIL
	{
		public static class Shape extends MINECART_DETECTOR.Shape
		{ }
	}
	
	public static class PRESSURE_PLATE_BINARY
	{
		public static class Powered extends _POWERED
		{ }
	}
	
	public static class PRESSURE_PLATE_WEIGHTED
	{
		public static class Power
		{
			public static IntData POWER(int power_0_15)
			{
				return new IntData("power", power_0_15, 15);
			}
		}
	}
	
	public static class PRISMARINE
	{
		public static class Variant
		{
			public static EnumData<EnumPrismarineVariant> VARIANT(EnumPrismarineVariant variant)
			{
				return new EnumData<EnumPrismarineVariant>("variant", variant);
			}
		}
	}
	
	public static class PUMPKIN
	{
		public static class Facing extends _FACING
		{ }
	}
	
	public static class QUARTZ
	{
		public static class Variant
		{
			public static EnumData<EnumQuartzVariant> VARIANT(EnumQuartzVariant variant)
			{
				return new EnumData<EnumQuartzVariant>("variant", variant);
			}
		}
	}
	
	public static class RED_FLOWERS
	{
		public static class Type extends BlockFlowers.Type
		{ }
	}

	public static class RED_SANDSTONE
	{
		public static class Type
		{
			public static EnumData<EnumRedSandstoneVariant> TYPE(EnumRedSandstoneVariant type)
			{
				return new EnumData<EnumRedSandstoneVariant>("type", type);
			}
		}
	}
	
	public static class REDSTONE_COMPERATOR
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Powered extends _POWERED
		{ }
		
		public static class Mode
		{
			public static EnumData<EnumComparatorMode> MODE(EnumComparatorMode mode)
			{
				return new EnumData<EnumComparatorMode>("mode", mode);
			}
		}
	}
	
	public static class REDSTONE_TORCH
	{
		public static class Facing extends TORCH.Facing
		{ }
	}
	
	public static class REDSTONE_WIRE
	{
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedEast
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedSouth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedWest
		{ }
		
		public static class Power
		{
			public static IntData POWER(int power_0_15)
			{
				return new IntData("power", power_0_15, 15);
			}
		}
	}
	
	public static class REED
	{
		public static class Age
		{
			public static IntData AGE(int age_0_15)
			{
				return new IntData("age", age_0_15, 15);
			}
		}
	}
	
	public static class REPEATER
	{
		public static class Locked
		{
			public static BoolData NOT_LOCKED = new BoolData("locked", false);
			public static BoolData LOCKED = new BoolData("locked", true);
		}
		
		public static class Delay
		{
			public static IntData DELAY(int delay_1_4)
			{
				return new IntData("delay", delay_1_4, 1, 4);
			}
		}
		
		public static class Facing extends _FACING_HORIZONTAL
		{ }
	}
	
	public static class SAND
	{
		public static class Variant
		{
			public static EnumData<EnumSandVariant> VARIANT(EnumSandVariant variant)
			{
				return new EnumData<EnumSandVariant>("variant", variant);
			}
		}
	}
	
	public static class SAND_STONE
	{
		public static class Type
		{
			public static EnumData<EnumSandstoneVariant> TYPE(EnumSandstoneVariant type)
			{
				return new EnumData<EnumSandstoneVariant>("type", type);
			}
		}
	}
	
	public static class SAPLING
	{
		public static class Type extends LOG.Variant
		{ }
		
		public static class Stage
		{
			public static IntData STAGE(int stage_0_1)
			{
				return new IntData("stage", stage_0_1, 1);
			}
		}
	}
	
	public static class SKULL
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class NoDrop
		{
			public static BoolData DROP = new BoolData("nodrop", false);
			public static BoolData NO_DROP = new BoolData("nodrop", true);
		}
	}

	public static class SMOOTH_BRICK
	{
		public static class Variant
		{
			public static EnumData<EnumStonebrickType> VARIANT(EnumStonebrickType variant)
			{
				return new EnumData<EnumStonebrickType>("variant", variant);
			}
		}
	}
	
	public static class SNOW
	{
		public static class Layers
		{
			public static IntData LAYERS(int layers_1_8)
			{
				return new IntData("layers", layers_1_8, 1, 8);
			}
		}
	}
	
	public static class SOIL
	{
		public static class Moisture
		{
			public static IntData MOISTURE(int moisture_0_7)
			{
				return new IntData("moisture", moisture_0_7, 7);
			}
		}
	}
	
	public static class SPONGE
	{
		public static class Wet
		{
			public static BoolData DRY = new BoolData("wet", false);
			public static BoolData WET = new BoolData("wet", true);
		}
	}
	
	public static class STAINED_GLASS
	{
		public static class Color extends _COLOR
		{ }
	}
	
	public static class STAINED_GLASS_PANE
	{
		public static class Color extends _COLOR
		{ }
		
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedEast
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedSouth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedWest
		{ }
	}
	
	public static class STAIRS
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Half
		{
			public static EnumData<EnumHalf> HALF(EnumHalf half)
			{
				return new EnumData<EnumHalf>("half", half);
			}
		}
		
		public static class Shape
		{
			public static EnumData<EnumStairShape> SHAPE(EnumStairShape shape)
			{
				return new EnumData<EnumStairShape>("shape", shape);
			}
		}
	}
	
	public static class STEM
	{
		public static class Age extends _PLANTS_AGE
		{ }
		
		public static class Facing
		{
			public static DirectionData NORTH = _FACING.NORTH;
			public static DirectionData EAST = _FACING.EAST;
			public static DirectionData SOUTH= _FACING.SOUTH;
			public static DirectionData WEST = _FACING.WEST;
			public static DirectionData UP = _FACING.UP;
		}
	}
	
	public static class STEP
	{
		public static class Seamless extends DOUBLE_STEP.Seamless
		{ }
		
		public static class Variant extends DOUBLE_STEP.Variant
		{ }
		
		public static class Half extends DOUBLE_STEP.Half
		{ }
	}
	
	public static class STONE
	{
		public static class Variant
		{
			public static EnumData<EnumStoneVariant> VARIANT(EnumStoneVariant variant)
			{
				return new EnumData<EnumStoneVariant>("variant", variant);
			}
		}
	}
	
	public static class TALL_PLANT
	{
		public static class Variant
		{
			public static EnumData<EnumTallFlowerVariants> VARIANT(EnumTallFlowerVariants variant)
			{
				return new EnumData<EnumTallFlowerVariants>("variant", variant);
			}
		}
		
		public static class Half
		{
			public static EnumData<EnumTallPlantHalf> HALF(EnumTallPlantHalf half)
			{
				return new EnumData<EnumTallPlantHalf>("half", half);
			}
		}
		
		public static class FACING extends _FACING_HORIZONTAL
		{ }
	}
	
	public static class THIN
	{
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedEast
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedSouth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedWest
		{ }
	}
	
	public static class TNT
	{
		public static class Explode
		{
			public static BoolData NOT_EXPLODE = new BoolData("explode", false);
			public static BoolData EXPLODE = new BoolData("explode", true);
		}
	}
	
	public static class TORCH
	{
		public static class Facing
		{
			public static DirectionData NORTH = _FACING.NORTH;
			public static DirectionData EAST = _FACING.EAST;
			public static DirectionData SOUTH = _FACING.SOUTH;
			public static DirectionData WEST = _FACING.WEST;
			public static DirectionData UP = _FACING.UP;
		}
	}
	
	public static class TRAPDOOR
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Open
		{
			public static BoolData CLOSED = new BoolData("open", false);
			public static BoolData OPEN = new BoolData("open", true);
		}
		
		public static class Half
		{
			public static EnumData<EnumTrapdoorHalf> HALF(EnumTrapdoorHalf half)
			{
				return new EnumData<EnumTrapdoorHalf>("half", half);
			}
		}
	}
	
	public static class TRIPWIRE
	{
		public static class Powered extends _POWERED
		{ }
		
		public static class Suspended
		{
			public static BoolData NOT_SUSPENDED = new BoolData("suspended", false);
			public static BoolData SUSPENDED = new BoolData("suspended", true);
		}
		
		public static class Attached
		{
			public static BoolData NOT_ATTACHED = new BoolData("attached", false);
			public static BoolData ATTACHED = new BoolData("attached", true);
		}
		
		public static class Disarmed
		{
			public static BoolData NOT_DISARMED = new BoolData("disarmed", false);
			public static BoolData DISARMED = new BoolData("disarmed", true);
		}
		
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedEast
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedSouth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedWest
		{ }
	}
	
	public static class TRIPWIRE_HOOK
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
		
		public static class Powered extends _POWERED
		{ }
		
		public static class Attached extends TRIPWIRE.Attached
		{ }
		
		public static class Suspended extends TRIPWIRE.Suspended
		{ }
	}
	
	public static class VINE
	{
		public static class ConnectedUp extends _CONNECTED.ConnectedUp
		{ }
		
		public static class ConnectedNorth extends _CONNECTED.ConnectedNorth
		{ }
		
		public static class ConnectedEast extends _CONNECTED.ConnectedEast
		{ }
		
		public static class ConnectedSouth extends _CONNECTED.ConnectedSouth
		{ }
		
		public static class ConnectedWest extends _CONNECTED.ConnectedWest
		{ }
	}
	
	public static class WALL_SIGN
	{
		public static class Facing extends _FACING_HORIZONTAL
		{ }
	}
	
	public static class WATER
	{
		public static class Level extends BlockFluid.Level
		{ }
	}
	
	public static class WOOD
	{
		public static class Variant
		{
			public static EnumData<EnumLogVariant> VARIANT(EnumLogVariant variant)
			{
				return new EnumData<EnumLogVariant>("variant", variant);
			}
		}
	}
	
	public static class YELLOW_FLOWERS
	{
		public static class Type extends BlockFlowers.Type
		{ }
	}
	
	
	public static class _PLANTS_AGE
	{
		public static IntData AGE(int age_0_7)
		{
			return new IntData("age", age_0_7, 7);
		}
	}
	
	public static class _CONNECTED
	{
		public static class ConnectedUp
		{
			public static BoolData NOT_CONNECTED = new BoolData("up", false);
			public static BoolData CONNECTED = new BoolData("up", true);
		}
		
		public static class ConnectedNorth
		{
			public static BoolData NOT_CONNECTED = new BoolData("north", false);
			public static BoolData CONNECTED = new BoolData("north", true);
		}
		
		public static class ConnectedEast
		{
			public static BoolData NOT_CONNECTED = new BoolData("east", false);
			public static BoolData CONNECTED = new BoolData("east", true);
		}
		
		public static class ConnectedSouth
		{
			public static BoolData NOT_CONNECTED = new BoolData("south", false);
			public static BoolData CONNECTED = new BoolData("south", true);
		}
		
		public static class ConnectedWest
		{
			public static BoolData NOT_CONNECTED = new BoolData("west", false);
			public static BoolData CONNECTED = new BoolData("west", true);
		}
	}
	
	public static class _FACING
	{
		static DirectionData NORTH = new DirectionData("facing", EnumDirection.NORTH);
		static DirectionData EAST = new DirectionData("facing", EnumDirection.EAST);
		static DirectionData SOUTH = new DirectionData("facing", EnumDirection.SOUTH);
		static DirectionData WEST = new DirectionData("facing", EnumDirection.WEST);
		static DirectionData UP = new DirectionData("facing", EnumDirection.UP);
		static DirectionData DOWN = new DirectionData("facing", EnumDirection.DOWN);
	}
	public static class _FACING_HORIZONTAL
	{
		public static final DirectionData NORTH = _FACING.NORTH;
		public static final DirectionData EAST = _FACING.EAST;
		public static final DirectionData SOUTH = _FACING.SOUTH;
		public static final DirectionData WEST = _FACING.WEST;
	}
	
	public static class _OPENED
	{
		public static BoolData CLOSED = new BoolData("open", false);
		public static BoolData OPEN = new BoolData("open", true);
	}
	
	public static class _POWERED
	{
		public static BoolData NOT_POWERED = new BoolData("powered", false);
		public static BoolData POWERED = new BoolData("powered", true);
	}
	
	public static class _ROTATION
	{
		public static final IntData SOUTH = new IntData("rotation", 0, 15);
		public static final IntData SOUTH_SOUTH_WEST = new IntData("rotation", 1, 15);
		public static final IntData SOUTH_WEST = new IntData("rotation", 2, 15);
		public static final IntData WEST_SOUTH_WEST = new IntData("rotation", 3, 15);
		public static final IntData WEST = new IntData("rotation", 4, 15);
		public static final IntData WEST_NORTH_WEST = new IntData("rotation", 5, 15);
		public static final IntData NORTH_WEST = new IntData("rotation", 6, 15);
		public static final IntData NORTH_NORTH_WEST = new IntData("rotation", 7, 15);
		public static final IntData NORTH = new IntData("rotation", 8, 15);
		public static final IntData NORTH_NORTH_EAST = new IntData("rotation", 9, 15);
		public static final IntData NORTH_EAST = new IntData("rotation", 10, 15);
		public static final IntData EAST_NORTH_EAST = new IntData("rotation", 11, 15);
		public static final IntData EAST = new IntData("rotation", 12, 15);
		public static final IntData EAST_SOUTH_EAST= new IntData("rotation", 13, 15);
		public static final IntData SOUTH_EAST = new IntData("rotation", 14, 15);
		public static final IntData SOUTH_SOUTH_EAST = new IntData("rotation", 15, 15);
	}
	
	public static class _COLOR
	{
		public static EnumData<EnumColor> COLOR(EnumColor color)
		{
			return new EnumData<EnumColor>("color", color);
		}
	}
	
	public static class _AXIS
	{
		public static EnumData<EnumAxis> X = new EnumData<EnumAxis>("axis", EnumAxis.X);
		public static EnumData<EnumAxis> Y = new EnumData<EnumAxis>("axis", EnumAxis.Y);
		public static EnumData<EnumAxis> Z = new EnumData<EnumAxis>("axis", EnumAxis.Z);
	}
}
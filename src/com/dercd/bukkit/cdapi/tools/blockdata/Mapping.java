package com.dercd.bukkit.cdapi.tools.blockdata;

import org.bukkit.block.BlockFace;

import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.collection.SyncedMap;

public class Mapping
{
	private static SyncedMap<BlockFace, DirectionData> BLOCKFACE_DIRECTIONDATA;
	private static SyncedMap<BlockFace, IntData> BLOCKFACE_ROTATION;
	private static Log clog;
	
	
	static
	{
		clog = Log.getInstance();
		clog.debug("Create mc mappings", null);
		init_BLOCKFACE_DIRECTIONDATA();
		init_BLOCKFACE_ROTATION();
		clog.debug("Mappings created", null);
	}
	
	
	private static void init_BLOCKFACE_DIRECTIONDATA()
	{
		clog.debug("Create mapping BLOCKFACE_DIRECTIONDATA", null);
		BLOCKFACE_DIRECTIONDATA = new SyncedMap<BlockFace, DirectionData>();
		BLOCKFACE_DIRECTIONDATA.put(BlockFace.DOWN, BlockVariant._FACING.DOWN);
		BLOCKFACE_DIRECTIONDATA.put(BlockFace.EAST, BlockVariant._FACING.EAST);
		BLOCKFACE_DIRECTIONDATA.put(BlockFace.NORTH, BlockVariant._FACING.NORTH);
		BLOCKFACE_DIRECTIONDATA.put(BlockFace.SOUTH, BlockVariant._FACING.SOUTH);
		BLOCKFACE_DIRECTIONDATA.put(BlockFace.UP, BlockVariant._FACING.UP);
		BLOCKFACE_DIRECTIONDATA.put(BlockFace.WEST, BlockVariant._FACING.WEST);
		clog.debug("Mapping BLOCKFACE_DIRECTIONDATA created", null);
	}
	
	private static void init_BLOCKFACE_ROTATION()
	{
		clog.debug("Create mapping BLOCKFACE_ROTATION", null);
		BLOCKFACE_ROTATION = new SyncedMap<BlockFace, IntData>();
		BLOCKFACE_ROTATION.put(BlockFace.EAST, BlockVariant._ROTATION.EAST);
		BLOCKFACE_ROTATION.put(BlockFace.EAST_NORTH_EAST, BlockVariant._ROTATION.EAST_NORTH_EAST);
		BLOCKFACE_ROTATION.put(BlockFace.EAST_SOUTH_EAST, BlockVariant._ROTATION.EAST_SOUTH_EAST);
		BLOCKFACE_ROTATION.put(BlockFace.NORTH, BlockVariant._ROTATION.NORTH);
		BLOCKFACE_ROTATION.put(BlockFace.NORTH_EAST, BlockVariant._ROTATION.NORTH_EAST);
		BLOCKFACE_ROTATION.put(BlockFace.NORTH_NORTH_EAST, BlockVariant._ROTATION.NORTH_NORTH_EAST);
		BLOCKFACE_ROTATION.put(BlockFace.NORTH_NORTH_WEST, BlockVariant._ROTATION.NORTH_NORTH_WEST);
		BLOCKFACE_ROTATION.put(BlockFace.NORTH_WEST, BlockVariant._ROTATION.NORTH_WEST);
		BLOCKFACE_ROTATION.put(BlockFace.SOUTH, BlockVariant._ROTATION.SOUTH);
		BLOCKFACE_ROTATION.put(BlockFace.SOUTH_EAST, BlockVariant._ROTATION.SOUTH_EAST);
		BLOCKFACE_ROTATION.put(BlockFace.SOUTH_SOUTH_EAST, BlockVariant._ROTATION.SOUTH_SOUTH_EAST);
		BLOCKFACE_ROTATION.put(BlockFace.SOUTH_SOUTH_WEST, BlockVariant._ROTATION.SOUTH_SOUTH_WEST);
		BLOCKFACE_ROTATION.put(BlockFace.SOUTH_WEST, BlockVariant._ROTATION.SOUTH_WEST);
		BLOCKFACE_ROTATION.put(BlockFace.WEST, BlockVariant._ROTATION.WEST);
		BLOCKFACE_ROTATION.put(BlockFace.WEST_NORTH_WEST, BlockVariant._ROTATION.WEST_NORTH_WEST);
		BLOCKFACE_ROTATION.put(BlockFace.WEST_SOUTH_WEST, BlockVariant._ROTATION.WEST_SOUTH_WEST);
		clog.debug("Mapping BLOCKFACE_ROTATION created", null);
	}
	
	
	public static DirectionData mapping_BLOCKFACE_DIRECTIONDATA(BlockFace blockFace)
	{
		return BLOCKFACE_DIRECTIONDATA.get(blockFace);
	}
	public static BlockFace mapping_BLOCKFACE_DIRECTIONDATA(DirectionData directionData)
	{
		return BLOCKFACE_DIRECTIONDATA.getOther(directionData);
	}
	
	public static IntData mapping_BLOCKFACE_ROTATION(BlockFace blockFace)
	{
		return BLOCKFACE_ROTATION.get(blockFace);
	}
	public static BlockFace mapping_BLOCKFACE_ROTATION(IntData intData)
	{
		return BLOCKFACE_ROTATION.getOther(intData);
	}
}

package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.IBlockData;
import net.minecraft.server.v1_9_R1.IBlockState;

public abstract class BlockVariantData<T extends Comparable<T>>
{
	protected IBlockState<T> ibs;
	protected T value;
	
	
	protected BlockVariantData(IBlockState<T> ibs, T value)
	{
		this.ibs = ibs;
		this.value = value;
	}
	
	
	public IBlockData applyTo(IBlockData ibd)
	{
		return ibd.set(this.ibs, this.value);
	}
	
	
	@Override
	public int hashCode()
	{
		int i = 5174;
		i *= this.getClass().hashCode() % i;
		i += (this.ibs == null ? 0 : this.ibs.hashCode());
		i -= (this.value == null ? 0 : this.value.hashCode());
		return i;
	}
}

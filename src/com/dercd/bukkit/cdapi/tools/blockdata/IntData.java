package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.BlockStateInteger;

public class IntData extends BlockVariantData<Integer>
{
	public IntData(String name, Integer value)
	{
		this(name, value, 255);
	}
	public IntData(String name, Integer value, int maxVal)
	{
		this(name, value, 0, maxVal);
	}
	public IntData(String name, Integer value, int minVal, int maxVal)
	{
		super(BlockStateInteger.of(name, minVal, maxVal), value);
	}
	
	
	public static IntData getNullData()
	{
		return NullData.IntNullData.get();
	}
}

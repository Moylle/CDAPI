package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.BlockStateDirection;
import net.minecraft.server.v1_9_R1.EnumDirection;

public class DirectionData extends BlockVariantData<EnumDirection>
{
	public DirectionData(String name, EnumDirection value)
	{
		//DEBUG
		super(BlockStateDirection.of(name, EnumDirection.EnumDirectionLimit.HORIZONTAL), value);
//		super(BlockStateDirection.of(name), value);
	}
	
	public static DirectionData getNullData()
	{
		return NullData.DirectionNullData.get();
	}
}

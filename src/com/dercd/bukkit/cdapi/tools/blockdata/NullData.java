package com.dercd.bukkit.cdapi.tools.blockdata;

import net.minecraft.server.v1_9_R1.IBlockData;
import net.minecraft.server.v1_9_R1.INamable;

public class NullData<T extends Comparable<T>> extends BlockVariantData<T>
{

	protected NullData()
	{
		super(null, null);
	}
	
	public IBlockData apply(IBlockData ibd)
	{
		return ibd;
	}
	
	
	public static <S extends Comparable<S>> NullData<S> get()
	{
		return new NullData<S>();
	}
	
	
	public static class DirectionNullData extends DirectionData
	{
		public DirectionNullData()
		{
			super(null, null);
		}
		
		public IBlockData apply(IBlockData ibd)
		{
			return ibd;
		}
		
		public static DirectionNullData get()
		{
			return new DirectionNullData();
		}
	}
	
	public static class EnumNullData<T extends Enum<T> & INamable> extends EnumData<T>
	{
		public EnumNullData()
		{
			super(null, null);
		}
		
		public IBlockData apply(IBlockData ibd)
		{
			return ibd;
		}
		
		public static <T extends Enum<T> & INamable> EnumNullData<T> get()
		{
			return new EnumNullData<T>();
		}
	}
	
	public static class BoolNullData extends BoolData
	{
		public BoolNullData()
		{
			super(null, false);
		}
		
		public IBlockData apply(IBlockData ibd)
		{
			return ibd;
		}
		
		public static BoolNullData get()
		{
			return new BoolNullData();
		}
	}
	
	public static class IntNullData extends IntData
	{
		public IntNullData()
		{
			super(null, null);
		}
		
		public IBlockData apply(IBlockData ibd)
		{
			return ibd;
		}
		
		public static IntNullData get()
		{
			return new IntNullData();
		}
	}
}

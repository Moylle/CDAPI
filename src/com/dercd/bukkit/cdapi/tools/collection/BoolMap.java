package com.dercd.bukkit.cdapi.tools.collection;

import java.util.HashMap;
import java.util.Map;

public class BoolMap<K> extends HashMap<K, Boolean>
{
	private static final long serialVersionUID = 5783529110784883394L;

	public BoolMap()
	{
	}
	public BoolMap(Map<K, Boolean> map)
	{
		super(map);
	}
	
	@Override
	public Boolean get(Object key)
	{
		Boolean back = super.get(key);
		return back == null ? false : (boolean) back;
	}
	
	@Override
	public Boolean put(K key, Boolean value)
	{
		Boolean back = super.put(key, value);
		return back == null ? false : (boolean) back;
	}
	
	@Override
	public Boolean remove(Object key)
	{
		Boolean back = super.remove(key);
		return back == null ? false : (boolean) back;
	}
}

package com.dercd.bukkit.cdapi.tools.collection;

import java.util.HashMap;
import java.util.Map;

import com.dercd.bukkit.cdapi.tools.Tools.Data;

public class TimeMap<K>
{
	private Map<K, Long> map = new HashMap<K, Long>();
	
	public void refresh(K key)
	{
		this.map.put(key, Data.getTimestamp());
	}
	public boolean check(K key, int interval)
	{
		if(this.map.get(key) == null) return true;
		return Data.getTimestamp() - this.map.get(key) >= interval;
	}
}

package com.dercd.bukkit.cdapi.tools.collection;

import java.util.HashMap;

public class CounterMap<K> extends HashMap<K, Integer>
{
	private static final long serialVersionUID = -1838137556012915184L;
	
	public void count(K key)
	{
		increment(key);
	}
	
	
	public Integer increment(K key)
	{
		return increment(key, 1);
	}
	public Integer increment(K key, int increment)
	{
		return put(key, get(key) + increment);
	}
	public Integer decrement(K key)
	{
		return increment(key, -1);
	}
	public Integer decrement(K key, int decrement)
	{
		return increment(key, -decrement);
	}
	
	public void reset(K key)
	{
		remove(key);
	}
	
	@Override
	public Integer get(Object key)
	{
		Integer back = super.get(key);
		return back == null ? 0 : back;
	}
}

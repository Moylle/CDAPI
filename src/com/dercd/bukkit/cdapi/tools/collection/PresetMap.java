package com.dercd.bukkit.cdapi.tools.collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PresetMap<K, V> extends HashMap<K, V>
{
	private static final long serialVersionUID = 4180244098866332095L;
	public V preset;
	public boolean useObjectCreator;
	public ObjectCreator objectCreator;
	Set<K> removed = new HashSet<K>();
	
	public PresetMap(V preset)
	{
		this.preset = preset;
		this.useObjectCreator = false;
	}
	public PresetMap(ObjectCreator objectCreator)
	{
		this.objectCreator = objectCreator;
		this.useObjectCreator = true;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key)
	{
		if(this.removed.contains(key) || super.containsKey(key)) return super.get(key);
		if(this.useObjectCreator)
		{
			V newObj = (V) this.objectCreator.newObject();
			put((K) key, newObj);
			return newObj;
		}
		return this.preset;
	}
	
	@Override
	public void clear()
	{
		super.clear();
		this.removed.clear();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V remove(Object key)
	{
		try { this.removed.add((K) key); }
		catch(ClassCastException x) {}
		return super.remove(key);
	}
	
	public V reset(Object key)
	{
		this.removed.remove(key);
		return super.remove(key);
	}
	
	public static interface ObjectCreator
	{
		public Object newObject();
	}
}

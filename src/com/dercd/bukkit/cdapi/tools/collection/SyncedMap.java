package com.dercd.bukkit.cdapi.tools.collection;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SyncedMap<K1, K2> extends HashMap<K1, K2>
{
	private static final long serialVersionUID = 4913913099342667669L;
	Map<K2, K1> map = new HashMap<K2, K1>();

	public SyncedMap()
	{
	}
	public SyncedMap(SyncedMap<K1, K2> map)
	{
		super(map);
		for (K1 key : map.keySet())
			this.map.put(map.get(key), key);
	}

	@Override
	public K2 put(K1 key, K2 val)
	{
		K2 back = super.put(key, val);
		if (back != null) this.map.remove(back);
		K1 k = this.map.put(val, key);
		if(k != null) super.remove(k);
		return back;
	}

	@Override
	public K2 remove(Object key)
	{
		K2 back = super.remove(key);
		if (back != null) this.map.remove(back);
		return back;
	}
	public K1 removeOther(K2 key)
	{
		K1 back = this.map.remove(key);
		if (back != null) super.remove(back);
		return back;
	}

	public boolean contains(K1 key1, K2 key2)
	{
		return super.containsKey(key1) || this.map.containsKey(key2);
	}
	public boolean containsOther(K2 key)
	{
		return this.map.containsKey(key);
	}

	public K1 getOther(K2 key)
	{
		return this.map.get(key);
	}

	public Set<Map.Entry<K2, K1>> entrySetOther()
	{
		return this.map.entrySet();
	}

	public Collection<K1> valuesOther()
	{
		return this.map.values();
	}

	public Set<K2> keySetOther()
	{
		return this.map.keySet();
	}

	@Override
	public boolean containsValue(Object value)
	{
		return this.map.containsKey(value);
	}

	@Override
	public void clear()
	{
		super.clear();
		this.map.clear();
	}

	@Override
	public SyncedMap<K1, K2> clone()
	{
		return new SyncedMap<K1, K2>(this);
	}
}

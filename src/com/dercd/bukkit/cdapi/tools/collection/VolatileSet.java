package com.dercd.bukkit.cdapi.tools.collection;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.dercd.bukkit.cdapi.tools.Tools.Data;

public class VolatileSet<K> extends HashSet<K>
{
	private static final long serialVersionUID = -1806448280068751627L;
	
	private Map<K, Long> map = new HashMap<K, Long>();
	private int lifetime;
	
	
	public VolatileSet(int lifetime)
	{
		this.lifetime = lifetime;
	}
	
	
	@Override
	public boolean add(K obj)
	{
		this.map.put(obj, Data.getTimestamp() + this.lifetime);
		refresh();
		return super.add(obj);
	}
	
	@Override
	public boolean addAll(Collection<? extends K> c)
	{
		long timestamp = Data.getTimestamp();
		for(K obj : c)
			this.map.put(obj, timestamp);
		refresh();
		return super.addAll(c);
	}
	
	@Override
	public void clear()
	{
		this.map.clear();
		super.clear();
	}
	
	@Override
	public boolean contains(Object obj)
	{
		refresh();
		return super.contains(obj);
	}
	
	@Override
	public boolean containsAll(Collection<?> c)
	{
		refresh();
		return super.containsAll(c);
	}
	
	@Override
	public boolean remove(Object obj)
	{
		refresh();
		this.map.remove(obj);
		return super.remove(obj);
	}
	
	@Override
	public boolean removeAll(Collection<?> c)
	{
		refresh();
		for(Object obj : c)
			this.map.remove(obj);
		return super.removeAll(c);
	}
	
	@Override
	public boolean retainAll(Collection<?> c)
	{
		refresh();
		return super.retainAll(c);
	}
	
	@Override
	public int size()
	{
		refresh();
		return super.size();
	}
	
	@Override
	public Object[] toArray()
	{
		refresh();
		return super.toArray();
	}
	
	@Override
	public <T> T[] toArray(T[] a)
	{
		refresh();
		return super.toArray(a);
	}
	
	public void refresh()
	{
		long timestamp = Data.getTimestamp();
		Iterator<Entry<K, Long>> i = this.map.entrySet().iterator();
		Entry<K, Long> e;
		while(i.hasNext())
		{
			e = i.next();
			if(e.getValue() < timestamp)
			{
				i.remove();
				super.remove(e.getKey());
			}
		}
	}
}

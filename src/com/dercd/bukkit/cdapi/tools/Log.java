package com.dercd.bukkit.cdapi.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.exceptions.Handleable;
import com.dercd.bukkit.cdapi.tools.Tools.Data;

public class Log
{
	public String path;
	public String latest;
	private Logger log;
	private LogStream p;
	protected Set<Object> debugMode = new HashSet<Object>();
	protected boolean globalDebugMode = false;
	
	
	public static enum LogLevel
	{
		INFO(""),
		DEBUG("/DEBUG"),
		WARN("/WARN"),
		ERROR("/ERROR");
		
		private final String suffix;
		
		private LogLevel(String suffix)
		{
			this.suffix = suffix;
		}
		
		@Override
		public String toString()
		{
			return this.suffix;
		}
	}
	
	
	@SuppressWarnings("resource")
	public Log(String path, PluginHandler handler)
	{
		this.path = path;
		this.latest = path + "/latest.log";
		this.log = handler.getLog();
		archive();
		File f = new File(path);
		try
		{
			if (!f.exists()) f.mkdirs();
			this.p = new LogStream(new FileOutputStream(new File(this.latest), true));
		}
		catch (Exception x)
		{}
		sort();
		checkDebug();
	}

	public String getCallingClass()
	{
		StackTraceElement[] stes = Thread.currentThread().getStackTrace();
		String className = null;
		for(int i = 2; i < stes.length && (className = stes[i].getClassName()).equals(getClass().getName()); i++);
		return className;
	}
	
	
	public void debug(String message, Object c)
	{
		log(message, c, LogLevel.DEBUG);
	}
	public void warn(String message, Object c)
	{
		log(message, c, LogLevel.WARN);
	}
	public void error(String message, Object c)
	{
		log(message, c, LogLevel.WARN);
	}
	public void log(String message)
	{
		log(message, null);
	}
	public void log(String message, Object c)
	{
		log(message, c, LogLevel.INFO);
	}
	public void log(String message, Object c, LogLevel level)
	{
		try
		{
			if(level == LogLevel.DEBUG && !this.globalDebugMode && !this.debugMode.contains(c)) return;
			String name = (c == null ? getCallingClass() : c.getClass().getName());
			name = name.substring(name.lastIndexOf('.') + 1) + ": ";
			this.p.println(name + message, level);
			this.p.flush();
		}
		catch (Exception x)
		{}
	}
	
	public void printException(Throwable t)
	{
		notifyAdmins(true);
		while (t != null)
		{
			if (t instanceof Handleable)
			{
				log("There are more nested exceptions which aren't print, because the current is Handleable", this);
				((Handleable) t).handle();
				break;
			}
			this.p.println("Exception: " + t.getClass().getName());
			t.printStackTrace(this.p);
			t = t.getCause();
		}
	}
	
	public void sort()
	{
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
		int[] data;
		File dir;
		for(File f : new ArrayList<File>(Arrays.asList(new File(this.path).listFiles())))
			try
			{
				if(f.isDirectory()) continue;
				data = split(f.getName());
				if(data == null) continue;
				if(data[0] == year && data[1] == month) continue;
				FileUtils.moveFileToDirectory(f, (dir = new File(this.path + "/" + data[0] + "-" + data[1])), !dir.exists());
			}
			catch (Exception x) { }
		for(File f : new ArrayList<File>(Arrays.asList(new File(this.path).listFiles())))
			try
			{
				if(!f.isDirectory()) continue;
				data = split(f.getName());
				if(data[0] < year)
					FileUtils.moveDirectoryToDirectory(f, (dir = new File(this.path + "/" + data[0])), !dir.exists());
			}
			catch (Exception x) { }
	}
	private int[] split(String fileName)
	{
		try
		{
			String[] s = fileName.split("-", 2);
			s[1] = s[1].split("-")[0];
			return new int[] { Integer.valueOf(s[0]), Integer.valueOf(s[1]) };
		}
		catch (Exception x)
		{
			return null;
		}
	}
	
	public PrintStream getStream()
	{
		return getStream(true);
	}
	public PrintStream getStream(boolean notifyAdmins)
	{
		if(notifyAdmins)
			notifyAdmins(false);
		return this.p;
	}
	
	public Consumer<String> getConsumer()
	{
		return getConsumer("");
	}
	public Consumer<String> getConsumer(String prefix)
	{
		return (s) -> { this.log(prefix + s); };
	}
	
	public void close()
	{
		log("Closing Stream", this);
		this.p.close();
	}

	private void notifyAdmins(boolean exception)
	{
		if(!exception)
		{
			for (Player p : Bukkit.getOnlinePlayers())
				if (p.hasPermission("cd.log.notifyerror")) p.sendMessage(ChatColor.DARK_RED + "[CDAPI] The LogStream is accessed by a CDPlugin. Probably an error occured");
			this.log.warning("[CDAPI] The LogStream is accessed by a CDPlugin. Probably an error occured.");
		}
		else
		{
			for (Player p : Bukkit.getOnlinePlayers())
				if (p.hasPermission("cd.log.notifyerror")) p.sendMessage(ChatColor.DARK_RED + "[CDAPI] A CDPlugin is printing an exception to the log. Probably an error occured.");
			this.log.warning("[CDAPI] A CDPlugin is printing an exception to the log. Probably an error occured.");
		}
	}

	private void checkDebug()
	{
		if(new File("./plugins/CDAPI/cdapi.debug").exists())
			enableGlobalDebug();
	}
	
	private void archive()
	{
		File f = new File(this.latest);
		if (!f.exists()) return;
		try
		{
			String outFilename;
			try
			{
				outFilename = getFileName(this.latest);
			}
			catch (Exception x)
			{
				corrupt(f);
				return;
			}
			String inFilename = this.latest;
			archive(inFilename, outFilename);
			f.delete();
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}
	}
	private void archive(String input, String output) throws IOException
	{
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(output))));
		BufferedReader bufferedReader = new BufferedReader(new FileReader(input));
		String line = null;
		while ((line = bufferedReader.readLine()) != null)
		{
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		}
		bufferedWriter.flush();
		bufferedWriter.close();
		bufferedReader.close();
	}
	
	private void corrupt(File f) throws IOException
	{
		archive(f.getPath(), CDPlugin.getDir() + "logs/" + getCorruptFileName());
		f.delete();
	}

	private String getCorruptFileName()
	{
		return "corrupt_archived_on_" + new java.text.SimpleDateFormat("yyyy_MM_dd_ss-mm-HH").format(new java.util.Date()) + ".gz";
	}

	private String getFileName(String path) throws Exception
	{
		byte[] read = new byte[19];
		int left = 19;
		try
		{
			InputStream fs = new FileInputStream(path);
			while (left > 0)
				left -= fs.read(read, 19 - left, left);
			String back = "";
			for (int i = 0; i < read.length; i++)
				back += (char) read[i];
			back = CDPlugin.getDir() + "logs/" + back.replace(":", "-").replace(' ', '_') + ".log.gz";
			fs.close();
			return back;
		}
		catch (Exception x)
		{
			System.out.println("[CDAPI] Error while archiving the latest log. It is now marked as corrupt");
			System.out.println("[CDAPI] Following the Stacktrace");
			x.printStackTrace();
			throw x;
		}
	}
	
	
	public void printStackTrace()
	{
		printStackTrace(Thread.currentThread());
	}
	public void printStackTrace(Thread t)
	{
		log("The current StackTrace for Thread \"" + t.getName() + "\" is printed now");
		Data.printStackTrace(this.p);
	}
	
	
	public boolean isDebugEnabled(Object o)
	{
		return this.debugMode.contains(o);
	}
	public boolean isDebug(Object o)
	{
		return isGlobalDebugEnabled() || isDebugEnabled(o);
	}
	public void disableDebug(Object o)
	{
		this.debugMode.remove(o);
	}
	public void enableDebug(Object o)
	{
		this.debugMode.add(o);
	}
	public boolean isGlobalDebugEnabled()
	{
		return this.globalDebugMode;
	}
	public void enableGlobalDebug()
	{
		this.globalDebugMode = true;
	}
	public void disableGlobalDebug()
	{
		this.globalDebugMode = false;
	}
	
	public static Log getInstance()
	{
		return CDAPI.getInstance().getHandler().getCLog();
	}

	
	public class LogStream extends PrintStream
	{
		
		public LogStream(OutputStream out) throws UnsupportedEncodingException
		{
			super(out, false, "UTF-8");
		}
		
		public void println(String s, LogLevel level)
		{
			s = getBeg(level) + s;
			super.println(s);
		}
		
		public void println(Object o, LogLevel level)
		{
			o = getBeg(level) + o;
			super.println(o);
		}
		
		@Override
		public void println(String s)
		{
			println(s, LogLevel.ERROR);
		}
		@Override
		public void println(Object o)
		{
			println(o, LogLevel.ERROR);
		}
		
		private String getBeg(LogLevel l)
		{
			return Data.getTime() + " [CDAPI" + l.toString() + "] ";
		}
	}
}	
	


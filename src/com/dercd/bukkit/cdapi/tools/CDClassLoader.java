package com.dercd.bukkit.cdapi.tools;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CDClassLoader extends URLClassLoader
{
	private Set<URL> urls;

	public CDClassLoader(URL[] urls, ClassLoader parent)
	{
		super(urls, parent);
		this.urls = new HashSet<URL>(Arrays.asList(urls));
	}

	@Override
	public void addURL(URL url)
	{
		if (containsURL(url)) return;
		this.urls.add(url);
		super.addURL(url);
	}

	public boolean containsURL(URL url)
	{
		return this.urls.contains(url);
	}
}

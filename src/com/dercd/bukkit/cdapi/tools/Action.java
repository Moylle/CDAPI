package com.dercd.bukkit.cdapi.tools;

public interface Action<T>
{
	public void run(T t);
}

package com.dercd.bukkit.cdapi.tools.minecraft;

import java.util.HashSet;
import java.util.Set;

import net.minecraft.server.v1_9_R1.NBTTagCompound;
import net.minecraft.server.v1_9_R1.NBTTagList;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class PlayerSnapshot
{
	public ItemStack[] invContents, armorContents;
	public Location location;
	public Vector velocity;
	public ItemStack[] enderchest;
	public PotionEffect[] potionEffects;
	public GameMode gameMode;
	public Integer itemSlot;
	public Boolean canFly;
	public Boolean isFlying;
	public Integer level;
	public Float xp;
	public Double health;
	public Integer foodLevel;
	
	private PlayerSnapshot()
	{
	}
	
	public void applyToPlayer(Player p)
	{
		if(this.invContents != null)
		{
			ItemStack[] playerInv = new ItemStack[36];
			for(int i = 0; i < 36; i++)
				playerInv[i] = this.invContents[i] == null ? null : this.invContents[i].clone();
			p.getInventory().setContents(playerInv);
			p.updateInventory();
		}
		if(this.armorContents != null)
		{
			ItemStack[] playerArmor = new ItemStack[4];
			for(int i = 0; i < 4; i++)
				playerArmor[i] = this.armorContents[i] == null ? null : this.armorContents[i].clone();
			p.getInventory().setArmorContents(playerArmor);
			p.updateInventory();
		}
		if(this.enderchest != null)
		{
			ItemStack[] playerEnderchest = new ItemStack[27];
			for(int i = 0; i < 27; i++)
				playerEnderchest[i] = this.enderchest[i] == null ? null : this.enderchest[i].clone();
			p.getEnderChest().setContents(playerEnderchest);
		}
		if(this.location != null)
			p.teleport(this.location.clone());
		if(this.velocity != null)
			p.setVelocity(this.velocity.clone());
		if(this.potionEffects != null)
		{
			PotionEffect[] potionEffects = new PotionEffect[p.getActivePotionEffects().size()];
			for(PotionEffect pe : p.getActivePotionEffects())
				p.removePotionEffect(pe.getType());
			for(PotionEffect potionEffect : potionEffects)
				p.addPotionEffect(potionEffect);
		}
		if(this.gameMode != null)
			p.setGameMode(this.gameMode);
		if(this.itemSlot != null && this.itemSlot >= 0 && this.itemSlot <= 8)
			p.getInventory().setHeldItemSlot(this.itemSlot);
		if(this.canFly != null)
			p.setAllowFlight(this.canFly);
		if(this.isFlying != null)
			p.setFlying(this.isFlying);
		if(this.level != null)
			p.setLevel(this.level);
		if(this.xp != null)
			p.setExp(this.xp);
		if(this.health != null)
			p.setHealth(this.health);
		if(this.foodLevel != null)
			p.setFoodLevel(this.foodLevel);
	}
	
	public NBTTagCompound toNBT()
	{
		NBTTagCompound base = new NBTTagCompound();
		base.set("invContents", Var.writeInventory(this.invContents));
		base.set("armorContents", Var.writeInventory(this.armorContents));
		base.set("location", Var.writeCoordsExact(this.location));
		base.set("velocity", Var.writeVectorExact(this.velocity));
		base.set("enderchest", Var.writeInventory(this.enderchest));
		NBTTagList list = new NBTTagList();
		for(PotionEffect pe : this.potionEffects)
			list.add(Var.writePotionEffect(pe));
		base.set("potionEffects", list);
		base.setString("gamemode", this.gameMode.name());
		base.setInt("itemSlot", this.itemSlot);
		base.setBoolean("canFly", this.canFly);
		base.setBoolean("isFlying", this.isFlying);
		base.setInt("level", this.level);
		base.setFloat("xp", this.xp);
		base.setDouble("health", this.health);
		base.setInt("foodLevel", this.foodLevel);
		return base;
	}
	
	public static PlayerSnapshot fromNBT(NBTTagCompound cpd)
	{
		try
		{
			PlayerSnapshot ps = new PlayerSnapshot();
			ps.invContents = Var.readInventory((NBTTagList) cpd.get("invContents"));
			ps.armorContents = Var.readInventory((NBTTagList) cpd.get("armorContents"));
			ps.location = Var.readCoords(cpd.getCompound("location"));
			ps.velocity = Var.readVector(cpd.getCompound("velocity"));
			ps.enderchest = Var.readInventory((NBTTagList) cpd.get("enderchest"));
			NBTTagList list = (NBTTagList) cpd.get("potionEffects");
			Set<PotionEffect> s = new HashSet<PotionEffect>();
			for(int i = 0; i < list.size(); ++i)
				s.add(Var.readPotionEffect(list.get(i)));
			ps.potionEffects = (PotionEffect[]) s.toArray();
			ps.gameMode = GameMode.valueOf(cpd.getString("gamemode"));
			ps.itemSlot = cpd.getInt("itemSlot");
			ps.canFly = cpd.getBoolean("canFly");
			ps.isFlying = cpd.getBoolean("isFlying");
			ps.level = cpd.getInt("level");
			ps.xp = cpd.getFloat("xp");
			ps.health = cpd.getDouble("health");
			ps.foodLevel = cpd.getInt("foodLevel");
			return ps;
		}
		catch(Exception x)
		{
			return null;
		}
	}
	
	public static PlayerSnapshot fromPlayer(Player p)
	{
		PlayerSnapshot ps = new PlayerSnapshot();
		ItemStack[] invContents = new ItemStack[36];
		ItemStack[] playerInv = p.getInventory().getContents();
		for(int i = 0; i < 36; i++)
			invContents[i] = playerInv[i] == null ? null : playerInv[i].clone();
		ItemStack[] armorContents = new ItemStack[4];
		ItemStack[] playerArmor = p.getInventory().getArmorContents();
		for(int i = 0; i < 4; i++)
			armorContents[i] = playerArmor[i] == null ? null : playerArmor[i].clone();
		ItemStack[] enderchest = new ItemStack[27];
		ItemStack[] playerEnderchest = p.getEnderChest().getContents();
		for(int i = 0; i < 27; i++)
			enderchest[i] = playerEnderchest[i] == null ? null : playerEnderchest[i].clone();
		Location location = p.getLocation().clone();
		Vector velocity = p.getVelocity().clone();
		PotionEffect[] potionEffects = new PotionEffect[p.getActivePotionEffects().size()];
		p.getActivePotionEffects().toArray(potionEffects);
		ps.invContents = invContents;
		ps.armorContents = armorContents;
		ps.location = location;
		ps.velocity = velocity;
		ps.enderchest = enderchest;
		ps.potionEffects = potionEffects;
		ps.gameMode = p.getGameMode();
		ps.itemSlot = p.getInventory().getHeldItemSlot();
		ps.canFly = p.getAllowFlight();
		ps.isFlying = p.isFlying();
		ps.level = p.getLevel();
		ps.xp = p.getExp();
		ps.health = ((Damageable) p).getHealth();
		ps.foodLevel = p.getFoodLevel();
		return ps;
	}
}

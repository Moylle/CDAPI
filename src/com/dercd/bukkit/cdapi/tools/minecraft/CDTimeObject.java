package com.dercd.bukkit.cdapi.tools.minecraft;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.dercd.bukkit.cdapi.tools.Tools.Data;

public class CDTimeObject
{
	private long global_cooldown;
	private long player_cooldown;
	private Map<UUID, Long> timestamps;
	public static UUID uuidGlobal = UUID.nameUUIDFromBytes("GLOBAL".getBytes());

	public CDTimeObject()
	{
		this.global_cooldown = -1;
		this.player_cooldown = -1;
		this.timestamps = new HashMap<UUID, Long>();
	}
	public CDTimeObject(int global_cooldown, int player_cooldown)
	{
		this.global_cooldown = global_cooldown;
		this.player_cooldown = player_cooldown;
		this.timestamps = new HashMap<UUID, Long>();
	}
	public CDTimeObject(int global_cooldown, int player_cooldown, Map<UUID, Long> timestamps)
	{
		this.global_cooldown = global_cooldown;
		this.player_cooldown = player_cooldown;
		this.timestamps = timestamps;
	}
	public CDTimeObject(int global_cooldown, long last_global_timestamp)
	{
		this.global_cooldown = global_cooldown;
		this.timestamps = new HashMap<UUID, Long>();
		this.timestamps.put(uuidGlobal, last_global_timestamp);
		this.player_cooldown = -1;
	}

	public void setGlobalCooldown(long l)
	{
		this.global_cooldown = l;
	}

	public void setPlayerCooldown(long player_cooldown)
	{
		this.player_cooldown = player_cooldown;
	}

	public void setTimestamps(Map<UUID, Long> timestamps)
	{
		this.timestamps = timestamps;
	}

	public long getGlobalCooldown()
	{
		return this.global_cooldown;
	}

	public long getPlayerCooldown()
	{
		return this.player_cooldown;
	}

	public void setTimestamp(UUID p, long l)
	{
		this.timestamps.put(p, l);
	}

	public void delPlayer(UUID p)
	{
		this.timestamps.remove(p);
	}

	public void clear()
	{
		this.timestamps.clear();
	}

	public long getTimestamp(UUID p)
	{
		Long l = this.timestamps.get(p);
		if (l != null) return l;
		return 0;
	}

	public Set<UUID> getOfflinePlayers()
	{
		return this.timestamps.keySet();
	}

	public void addTimestampData(List<String> list)
	{
		list.add("   PlayerCooldown: " + this.getPlayerCooldown());
		list.add("   GlobalCooldown: " + this.getGlobalCooldown());
		list.add("      Time to next global use (in seconds): " + this.getTimeToNextGlobalUse());
	}

	public boolean checkCooldown(UUID p)
	{
		long timestamp = Data.getTimestamp();
		return ((this.global_cooldown == -1 || timestamp - getTimestamp(uuidGlobal) > this.global_cooldown) && (this.player_cooldown == -1 || timestamp - getTimestamp(p) > this.player_cooldown));
	}

	public long getTimeToNextGlobalUse()
	{
		if (this.global_cooldown == -1 || this.timestamps.get(uuidGlobal) == null) return 0;
		long i = this.global_cooldown - (Data.getTimestamp() - this.getTimestamp(uuidGlobal)) + 1;
		return (i < 0 ? 0 : i);
	}

	public long getTimeToNextUse(UUID p)
	{
		long i;
		if (this.player_cooldown == -1 || this.timestamps.get(p) == null) i = 0;
		else i = this.player_cooldown - (Data.getTimestamp() - this.getTimestamp(p)) + 1;
		i = Math.max(i, this.getTimeToNextGlobalUse());
		return (i < 0 ? 0 : i);
	}
	
	@Override
	public CDTimeObject clone()
	{
		CDTimeObject o = new CDTimeObject();
		clone(o);
		return o;
	}
	
	protected void clone(CDTimeObject o)
	{
		o.global_cooldown = this.global_cooldown;
		o.player_cooldown = this.player_cooldown;
		o.timestamps = new HashMap<UUID, Long>(this.timestamps);
	}
}

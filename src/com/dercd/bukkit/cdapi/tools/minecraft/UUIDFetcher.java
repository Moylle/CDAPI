package com.dercd.bukkit.cdapi.tools.minecraft;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerNameNotFoundException;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerResolveException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Minecraft;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import org.bukkit.entity.Player;

public class UUIDFetcher
{
	private Log clog = Log.getInstance();
	private static final double PROFILES_PER_REQUEST = 100;
	private static final String PROFILE_URL = "https://api.mojang.com/profiles/minecraft";
	private JsonParser jsonParser = new JsonParser();
	protected Map<String, UUID> fetched = new HashMap<String, UUID>();
	private NameFetcher nameFetcher;
	
	public UUIDFetcher()
	{
		init();
	}
	
	public void init()
	{
		for(Player p : Minecraft.getOnlinePlayers())
			this.fetched.put(p.getName(), p.getUniqueId());	
	}
	
	public void setNameFetcher(NameFetcher f)
	{
		this.nameFetcher = f;
	}

	public Map<String, UUID> fetch(List<String> input) throws CDPlayerResolveException
	{
		return fetch(input, 0);
	}

	public Map<String, UUID> fetch(List<String> input, int attempt) throws CDPlayerResolveException
	{
		List<String> names = new ArrayList<String>(input);
		for (String s : input)
			if (this.fetched.containsKey(s) && this.fetched.get(s) != null) names.remove(s);
		int requests = (int) Math.ceil(names.size() / PROFILES_PER_REQUEST);
		for (int i = 0; i < requests; i++)
		{
			try
			{
				HttpURLConnection connection = createConnection();
				JsonArray ja = new JsonArray();
				for (String str : names.subList(i * 100, Math.min((i + 1) * 100, names.size())))
					ja.add(new JsonPrimitive(str));
				String body = ja.toString();
				writeBody(connection, body);
				JsonArray array = (JsonArray) this.jsonParser.parse(new InputStreamReader(connection.getInputStream()));
				JsonObject jsonProfile;
				Iterator<JsonElement> it = array.iterator();
				while (it.hasNext())
				{
					jsonProfile = (JsonObject) it.next();
					String id = jsonProfile.get("id").getAsString();
					String name = jsonProfile.get("name").getAsString();
					UUID uuid = UUIDFetcher.getUUID(id);
					this.fetched.put(name, uuid);
					this.nameFetcher.fetched.put(uuid, name);
				}
				attempt = 0;
			}
			catch (Exception x)
			{
				attempt++;
				this.clog.log("Error while fetching UUIDs; " + attempt + " attempt", this);
				if (attempt >= 5)
				{
					this.clog.log("Do not try to fetch UUIDs anymore", this);
					CDPlayerResolveException toThrow = new CDPlayerResolveException();
					toThrow.initCause(x);
					throw toThrow;
				}
				return fetch(input, attempt);
			}
		}
		return ImmutableMap.copyOf(this.fetched);
	}

	private static void writeBody(HttpURLConnection connection, String body) throws Exception
	{
		OutputStream stream = connection.getOutputStream();
		stream.write(body.getBytes());
		stream.flush();
		stream.close();
	}

	private static HttpURLConnection createConnection() throws IOException
	{
		URL url = new URL(PROFILE_URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setReadTimeout(5000);
		connection.setConnectTimeout(5000);
		return connection;
	}

	private static UUID getUUID(String id)
	{
		return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-" + id.substring(20, 32));
	}

	public UUID fetch(String name) throws CDPlayerResolveException
	{
		try
		{
			UUID uuid = fetch(new ArrayList<String>(Arrays.asList(name))).get(name);
			if(uuid != null) return uuid;
			throw new CDPlayerNameNotFoundException(name);
		}
		catch (Exception x)
		{
			if (x instanceof CDPlayerResolveException) throw (CDPlayerResolveException) x;
			this.clog.printException(x);
			CDPlayerResolveException toThrow = new CDPlayerResolveException(name);
			toThrow.initCause(x);
			throw toThrow;
		}
	}

	public void put(String name, UUID u)
	{
		this.fetched.put(name, u);
		this.nameFetcher.fetched.put(u, name);
	}

	public static UUIDFetcher getInstance()
	{
		return CDAPI.getInstance().getHandler().getUUIDFetcher();
	}
}

package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDPlayerNameNotFoundException extends CDPlayerResolveException
{
	private static final long serialVersionUID = -1634776040191454653L;
	public String player;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The Player you've typed in was not found. Please type in the full name and mind that it's case-sensitve";

	public CDPlayerNameNotFoundException()
	{
	}

	public CDPlayerNameNotFoundException(String player)
	{
		this.player = player;
		this.message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The Player '" + player + "' wasn't found. Please type in the full name and mind that it's case-sensitve";
	}

	public CDPlayerNameNotFoundException(String player, String message)
	{
		this.player = player;
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		e.getSender().sendMessage(this.message);
	}
}

package com.dercd.bukkit.cdapi.exceptions;

import com.dercd.bukkit.cdapi.PluginHandler;

public class CDUnsupportedPacketModifierException extends CDCException
{
	private static final long serialVersionUID = -741004066139815715L;
	private Object o;

	public CDUnsupportedPacketModifierException(Object o)
	{
		this.o = o;
	}

	@Override
	public void handle(PluginHandler handler)
	{
		handler.getCLog().log("Tryed to save an usupported Object from the Modifier of a Packet (" + this.o.getClass().getName() + ")", this);
	}

	public Object getObject()
	{
		return this.o;
	}
}

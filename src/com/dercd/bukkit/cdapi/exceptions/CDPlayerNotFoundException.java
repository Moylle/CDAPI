package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDPlayerNotFoundException extends CDCException
{
	private static final long serialVersionUID = -4176205230675388510L;
	private String player;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The Player '" + this.player + "' was not found";

	public CDPlayerNotFoundException()
	{
		this.message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The Player you've typed in was not found";
	}

	public CDPlayerNotFoundException(String player)
	{
		this.player = player;
	}

	public CDPlayerNotFoundException(String player, String message)
	{
		this.player = player;
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e) throws CDInvalidArgsException
	{
		e.getSender().sendMessage(this.message);
		throw new CDInvalidArgsException(e.getCommand().getName());
	}
}

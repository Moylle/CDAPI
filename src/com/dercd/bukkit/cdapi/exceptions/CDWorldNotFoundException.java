package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDWorldNotFoundException extends CDCException
{
	private static final long serialVersionUID = 4162951419658392695L;
	private String world;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The World '" + this.world + "' was not found";

	public CDWorldNotFoundException(String world)
	{
		this.world = world;
	}

	public CDWorldNotFoundException(String world, String message)
	{
		this.world = world;
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e) throws CDInvalidArgsException
	{
		e.getSender().sendMessage(this.message);
		throw new CDInvalidArgsException(e.getCommand().getName());
	}
}

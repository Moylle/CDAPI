package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDNoIOPermissionException extends CDNoPermissionException
{
	private static final long serialVersionUID = 7758122512798498814L;
	public boolean notify;
	public String message = Tools.Var.getExclamation(ChatColor.RED) + ChatColor.DARK_RED + "You are not allowed to perform IO-Actions. Probably data is saved in RAM but couldn't write to disk";
	
	public CDNoIOPermissionException()
	{
	}
	
	public CDNoIOPermissionException(String message)
	{
		super(message);
	}

	public CDNoIOPermissionException(boolean notify)
	{
		super(notify);
	}

	public CDNoIOPermissionException(boolean notify, String message)
	{
		super(notify, message);
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		handler.getCLog().log(e.getSender().getName() + " was denyed to perform an IO-Action with command '" + e.getCommand().getName() + " " + Var.arrToString(e.getArgs(), 0) + "'. The Player was " + (this.notify ? "" : "not ") + "notifyed", this);
		if (this.notify) e.getSender().sendMessage(this.message);
	}
	
}

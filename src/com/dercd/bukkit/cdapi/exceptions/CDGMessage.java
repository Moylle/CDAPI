package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.entity.Player;

import com.dercd.bukkit.cdapi.tools.Log;

public class CDGMessage extends CDGException implements Handleable
{
	private static final long serialVersionUID = 5053518929310115469L;
	private String message;
	private Log clog;

	public CDGMessage(Player p, String message, Log clog)
	{
		super(p);
		this.message = message;
		this.clog = clog;
	}
	public CDGMessage(Player p, String message)
	{
		this(p, message, Log.getInstance());
	}

	@Override
	public void handle()
	{
		this.p.sendMessage(this.message);
		this.clog.log("GeneralException (MessageException) thrown. Message: " + this.message, this);
	}
}

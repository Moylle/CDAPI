package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.entity.Player;

public abstract class CDGException extends CDException implements Handleable
{
	private static final long serialVersionUID = -497458546149267818L;
	Player p;

	public CDGException(Player p)
	{
		this.p = p;
	}

	@Override
	public abstract void handle();
}

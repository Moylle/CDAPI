package com.dercd.bukkit.cdapi.exceptions;

import java.util.UUID;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDPlayerUUIDNotFoundException extends CDPlayerResolveException
{
	private static final long serialVersionUID = -1634776040191454653L;
	public String uuidStr;
	public UUID uuid;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The UUID you've typed in was not found. Please type in the full UUID and mind the dashes";

	public CDPlayerUUIDNotFoundException()
	{
	}
	
	public CDPlayerUUIDNotFoundException(UUID uuid)
	{
		this.uuid = uuid;
		this.message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The UUID " + uuid + " wasn't found";
	}
	
	public CDPlayerUUIDNotFoundException(UUID uuid, String message)
	{
		this.uuid = uuid;
		this.message = message;
	}
	
	public CDPlayerUUIDNotFoundException(String uuid)
	{
		this.uuidStr = uuid;
		this.message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The UUID " + uuid + " wasn't found. Please type in the full UUID and mind the dashes";
	}

	public CDPlayerUUIDNotFoundException(String uuid, String message)
	{
		this.uuidStr = uuid;
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		e.getSender().sendMessage(this.message);
	}
}

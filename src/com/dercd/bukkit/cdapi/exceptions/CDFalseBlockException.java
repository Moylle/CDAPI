package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDFalseBlockException extends CDSelectionException
{
	private static final long serialVersionUID = -8417329574085048660L;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.GOLD + "You have selected the wrong block(s)";

	public CDFalseBlockException()
	{
		setMessage(this.message);
	}

	public CDFalseBlockException(String message)
	{
		this.message = message;
		setMessage(message);
	}

	public CDFalseBlockException(Material m)
	{
		this.message += ". You have to select (a) block(s) of Type " + m.name();
		setMessage(this.message);
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		e.getSender().sendMessage(this.message);
	}
}

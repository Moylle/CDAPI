package com.dercd.bukkit.cdapi.exceptions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDGNoIOPermissionException extends CDGNoPermissionException
{
	private static final long serialVersionUID = -2300386686996531570L;
	public String message = Var.getExclamation(ChatColor.RED) + ChatColor.DARK_RED + "You are not allowed to perform IO-Actions. Probably data is saved in RAM but couldn't write to disk";
	private Log clog;
	
	
	public CDGNoIOPermissionException(Player p)
	{
		this(p, Log.getInstance());
	}
	public CDGNoIOPermissionException(Player p, String message)
	{
		this(p, message, Log.getInstance());
	}
	public CDGNoIOPermissionException(Player p, Log clog)
	{
		super(p, clog);
	}
	public CDGNoIOPermissionException(Player p, String message, Log clog)
	{
		super(p, message, clog);
	}

	@Override
	public void handle()
	{
		this.p.sendMessage(this.message);
		this.clog.log("GeneralException (NoIOPermissionException) thrown. Message: " + this.message, this);
	}
}

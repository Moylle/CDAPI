package com.dercd.bukkit.cdapi.exceptions;

import java.util.UUID;

import org.bukkit.ChatColor;

import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools.Var;

public class CDPlayerResolveException extends CDResolveException
{
	private static final long serialVersionUID = -3591861875315869314L;
	public String player;
	public UUID uuid;
	public String message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The given Player or UUID could not be resolved. Probably the Mojang-Session-Server does not response";

	public CDPlayerResolveException()
	{
	}

	public CDPlayerResolveException(String player)
	{
		this.player = player;
		this.message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The given Player could not be resolved. Probably the Mojang-Session-Server does not response";
	}

	public CDPlayerResolveException(UUID uuid)
	{
		this.uuid = uuid;
		this.message = Var.getExclamation(ChatColor.GOLD) + ChatColor.RED + "The given UUID could not be resolved. Probably the Mojang-Session-Server does not response";
	}

	public CDPlayerResolveException(String player, String message)
	{
		this.player = player;
		this.message = message;
	}

	public CDPlayerResolveException(UUID uuid, String message)
	{
		this.uuid = uuid;
		this.message = message;
	}

	@Override
	public void handle(PluginHandler handler, CommandEvent e)
	{
		e.getSender().sendMessage(this.message);
	}
}

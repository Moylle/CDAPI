package com.dercd.cdio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public abstract class CDConnection extends LogPrinter
{
	public ConnectionCloseDelegate onClosed;
	public OnBeforeKeywordDelegate onBeforeKeyword;	
	
	protected Socket socket;
	protected InputStream inputStream;
	protected OutputStream outputStream;
	
	public String keyWordIn;
	public String keyWordOut;
	protected int readCount = 1000;
	protected byte[] buffer;
	protected boolean newReadLength = false;
	public CDProtocol protocol;
	public Thread t;
	public ThreadGroup threadGroup;
	protected InetAddress address;
	public Charset encoding = Charset.forName("UTF-8");
	public String name;
	
	public static Charset ascii = Charset.forName("ASCII");
	private static final MessageDigest md5;
	
	static
	{
		MessageDigest md;
		try
		{
			md = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException x)
		{
			//Won't happen
			md = null;
			x.printStackTrace();
		}
		md5 = md;
	}
	
	public CDConnection()
	{
		this(null, null);
	}
	public CDConnection(byte[] keyWordIn, byte[] keyWordOut)
	{
		this.buffer = new byte[this.readCount];
		setKeyWordIn(keyWordIn);
		setKeyWordOut(keyWordOut);
	}
	
	protected void callOnClosed(CloseReason reason)
	{
		if (this.onClosed != null)
			this.onClosed.onClosed(this, reason);
	}
	
	public CDProtocol createProtocol() throws Exception
	{
		this.protocol = new CDProtocol(this.socket, 10000, 1048576, true);
		this.protocol.setKeyword(this.keyWordIn);
		this.protocol.out = this.out;
		return this.protocol;
	}
	
	public void send(String s) throws IOException
	{
		if (!this.isAlive())
			throw new IOException("Connection isn't alive");
		this.protocol.send(s);
	}
	public void sendRaw(byte[] b) throws IOException
	{
		sendRaw(b, true, 0, 1);
	}
	public void sendRaw(byte[] b, boolean addHeader) throws IOException
	{
		sendRaw(b, addHeader, 0, 1);
	}
	public void sendRaw(byte[] b, int start) throws IOException
	{
		sendRaw(b, true, start, 1);
	}
	public void sendRaw(byte[] b, boolean addHeader, int start) throws IOException
	{
		sendRaw(b, addHeader, start, 1);
	}
	public void sendRaw(byte[] b, int start, int length) throws IOException
	{
		sendRaw(b, true, start, length);
	}
	public void sendRaw(byte[] b, boolean addHeader, int start, int length) throws IOException
	{
		if (isAlive())
			throw new IOException("Connection isn't alive");
		this.protocol.sendRaw(b, addHeader, start, length, OptionMask.RAW);
	}
	
	public abstract void close(CloseReason reason);
	
	public void interrupt()
	{
		close(CloseReason.THREAD_INTERRUPTED);
		if (this.t != null)
			this.t.interrupt();
	}
	
	public void setKeyWordIn(byte[] keyWordIn)
	{
		this.keyWordIn = hash(keyWordIn != null ? keyWordIn : new byte[0]);
	}
	public void setKeyWordOut(byte[] keyWordOut)
	{
		this.keyWordOut = hash(keyWordOut != null ? keyWordOut : new byte[0]);
	}
	
	public InetAddress getInetAddress()
	{
		return this.address;
	}
	public String getIPStr()
	{
		return getIPString();
	}
	public String getIPString()
	{
		return this.socket == null ? "/" : this.socket.getInetAddress().getHostAddress() + ":" + this.socket.getPort();
	}
	public String getThreadName()
	{
		return this.t != null ? this.t.getName() : "";
	}
	public void setThreadName(String value)
	{
		if (this.t != null && value != null && value.length() != 0)
			this.t.setName(value);
	}
	public int getReadCount()
	{
		return this.readCount;
	}
	public void setReadCount(int value)
	{
		synchronized (this)
		{
			this.readCount = value;
			this.buffer = new byte[value];
		}
	}
	public boolean isAlive()
	{
		return isProtocolSet() && this.protocol.isAlive() && this.t != null && this.t.isAlive();
	}
	public boolean isProtocolSet()
	{
		return this.protocol != null;
	}
	
	protected void listen() throws IOException
	{
		int read;
//		BufferedReader reader = new BufferedReader(new InputStreamReader(this.inputStream));
		while (true)
		{
			synchronized (this)
			{
				if ((read = this.inputStream.read(this.buffer, 0, this.readCount)) < 0)
					break;
				else if(read == 0)
					continue;
			}
			this.protocol.process(this.buffer, read);
		}
	}
	
	public void sendKeyword() throws IOException
	{
		onBeforeKeyWord();
		println("Sending keyword '" + this.keyWordOut + "'");
		byte[] b = this.keyWordOut.getBytes(ascii);
		this.outputStream.write(b, 0, b.length);
		this.outputStream.flush();
		println("Keyword sent");
	}
	public void onBeforeKeyWord()
	{
		if(this.onBeforeKeyword != null)
			this.onBeforeKeyword.run(this);
	}
	
	@Override
	protected void println(String s)
	{
		printlnRaw((this instanceof CDIncommingConnection ? "CDIC" : "CDOC") + (this.name != null ? "," + this.name : "") + "[" + getIPString() + "]: " + s);
	}
	
	protected static String hash(byte[] keyWord)
	{
		synchronized (md5)
		{
			md5.reset();
			StringBuilder sb = new StringBuilder();
		    for (byte b : md5.digest(keyWord))
		        sb.append(String.format("%02X", b));
		    return sb.toString();
		}
	}
}

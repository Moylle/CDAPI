package com.dercd.cdio;

import java.io.IOException;
import java.net.Socket;

public interface CDICCreator
{
	public CDIncommingConnection create(Socket s, byte[] keyWordIn, byte[] keyWordOut) throws IOException;
}

package com.dercd.cdio;

public interface ConnectionOpenDelegate
{
	public void onConnected(CDOutgoingConnection cdoc);
}

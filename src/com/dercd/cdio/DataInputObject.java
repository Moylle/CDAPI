package com.dercd.cdio;

public interface DataInputObject
{
	public void input(String input);
}

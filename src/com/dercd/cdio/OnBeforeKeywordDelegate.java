package com.dercd.cdio;

public interface OnBeforeKeywordDelegate
{
	public void run(CDConnection cdc);
}

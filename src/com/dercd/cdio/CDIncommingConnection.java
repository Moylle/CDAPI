package com.dercd.cdio;

import java.io.IOException;
import java.net.Socket;

public class CDIncommingConnection extends CDConnection
{
	public CDIncommingConnection(Socket socket) throws IOException
	{
		this(socket, null, null, null, null);
	}
	public CDIncommingConnection(Socket socket, byte[] keyWordIn, byte[] keyWordOut, CDProtocol cdp, ThreadGroup threadGroup) throws IOException
	{
		super(keyWordIn, keyWordOut);
		this.protocol = cdp;
		this.socket = socket;
		this.inputStream = socket.getInputStream();
		this.outputStream = socket.getOutputStream();
		this.address = this.socket.getInetAddress();
		this.threadGroup = threadGroup;
	}
	
	public boolean isVerified()
	{
		return isProtocolSet() ? this.protocol.isVerifyed() : true;
	}
	public boolean isActive()
	{
		return this.socket != null && this.socket.isConnected();
	}
	
	public void run()
	{
		if(this.t != Thread.currentThread())
		{
			this.t = new Thread(this.threadGroup, () -> { run(); });
			this.t.setName("CDIC-Thread");
			this.t.start();
			synchronized (this)
			{
				try
				{
					this.wait();
				}
				catch (InterruptedException x)
				{
					printException(x);
					x.printStackTrace();
				}
			}
			return;
		}
		try
		{
			println("New connection");
			if (!isProtocolSet())
				createProtocol();
			synchronized (this)
			{
				this.notifyAll();
			}
			sendKeyword();
			listen();
			close(CloseReason.IC_CON_CLOSED);
		}
		catch (InterruptedException x)
		{
			close(CloseReason.THREAD_INTERRUPTED);
		}
		catch (Throwable t)
		{
			printException(t);
			close(CloseReason.IC_CON_CRASHED);
		}
	}

	@Override
	public void close(CloseReason reason)
	{
		try
		{
			this.socket.close();			
		}
		catch (Throwable t)
		{ }
		try
		{
			println("Closed (" + reason.name() + ")");
			callOnClosed(reason);
		}
		catch (Throwable t)
		{ }
	}
}